mod common;

use chrono::{TimeZone, Utc};
use indoc::indoc;
use ncmprs::client::types::*;
use ncmprs::client::*;

#[test]
fn clearerror() {
    assert_cmd!(ClearError, b"clearerror\n", "OK\n", Response::Ok);
}

#[test]
fn currentsong() {
    assert_cmd!(
        CurrentSong,
        b"currentsong\n",
        &format!("{}\nOK\n", *common::SONG_RES_0),
        common::SONG_0.clone()
    );
}

#[test]
fn idle() {
    assert_cmd!(
        Idle(Subsystems::all()),
        b"idle\n",
        "changed: database\nchanged: update\nOK\n",
        IdleResponse {
            changed: Subsystems::DATABASE | Subsystems::UPDATE
        }
    );

    assert_cmd!(
        Idle(Subsystems::all()),
        b"idle\n",
        "OK\n",
        IdleResponse {
            changed: Subsystems::empty()
        }
    );

    assert_cmd!(
        Idle(Subsystems::DATABASE | Subsystems::UPDATE),
        b"idle database update\n",
        "OK\n",
        IdleResponse {
            changed: Subsystems::empty()
        }
    );
}

#[test]
fn status() {
    let response_text = indoc!(
        "volume: 62
         repeat: 0
         random: 0
         single: 0
         consume: 0
         playlist: 2
         playlistlength: 14
         mixrampdb: 0.000000
         state: pause
         song: 0
         songid: 1
         time: 32:199
         elapsed: 31.873
         bitrate: 266
         duration: 199.367
         audio: 44100:f:2
         nextsong: 1
         nextsongid: 2
         OK
         "
    );

    let response = StatusResponse {
        volume: 62,
        repeat: false,
        random: false,
        single: SingleMode::Off,
        consume: false,
        playlist: 2,
        playlist_len: 14,
        mixrampdb: 0.0,
        mixrampdelay: 0.0,
        state: PlayerState::Pause,
        song_index: SongIndex(0),
        song_id: SongId(1),
        elapsed: 31.873,
        bitrate: 266,
        duration: 199.367,
        audio_format: AudioFormat {
            sample_rate: 44100,
            bits: AudioBits::Float32,
            channels: 2,
        },
        next_song_index: Some(SongIndex(1)),
        next_song_id: Some(SongId(2)),
        crossfade: 0,
        error: None,
        updating_db: None,
    };

    assert_cmd!(Status, b"status\n", response_text, response);
}

#[test]
fn stats() {
    let response_text = indoc!(
        "uptime: 26238
         playtime: 508
         artists: 79
         albums: 650
         songs: 8002
         db_playtime: 1943109
         db_update: 1566037410
         OK
         "
    );

    let response = StatsResponse {
        uptime: 26238,
        playtime: 508,
        artists: 79,
        albums: 650,
        songs: 8002,
        db_playtime: 1943109,
        db_update: Utc.timestamp(1566037410, 0),
    };

    assert_cmd!(Stats, b"stats\n", response_text, response);
}
