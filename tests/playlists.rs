mod common;

use chrono::{TimeZone, Utc};
use ncmprs::client::types::*;
use ncmprs::client::*;

#[test]
fn listplaylistinfo() {
    assert_cmd!(
        PlaylistList {
            playlist: "radiohead".into()
        },
        b"listplaylistinfo radiohead\n",
        &common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );
}

#[test]
fn listplaylist() {
    assert_cmd!(
        PlaylistListShort {
            playlist: "radiohead".into()
        },
        b"listplaylist radiohead\n",
        b"file: Radiohead/The Bends [1995]/01 Planet Telex.m4a\nfile: Radiohead/The Bends [1995]/02 The Bends.m4a\nOK\n",
        FileList(vec![
            "Radiohead/The Bends [1995]/01 Planet Telex.m4a".into(),
            "Radiohead/The Bends [1995]/02 The Bends.m4a".into()
        ])
    );
}

#[test]
fn listplaylists() {
    assert_cmd!(
        PlaylistsList,
        b"listplaylists\n",
        indoc::indoc!(
            "playlist: kid a
            Last-Modified: 2019-03-09T20:12:11Z
            playlist: amnesiac
            Last-Modified: 2019-02-07T17:42:34Z
            OK
            "
        ),
        PlaylistsListResponse(vec![
            PlaylistEntry {
                name: "kid a".into(),
                last_modified: Utc.ymd(2019, 3, 9).and_hms(20, 12, 11),
            },
            PlaylistEntry {
                name: "amnesiac".into(),
                last_modified: Utc.ymd(2019, 2, 7).and_hms(17, 42, 34),
            }
        ])
    );
}

#[test]
fn load() {
    assert_cmd_ok!(
        PlaylistLoad {
            playlist: "amnesiac".into(),
            range: None
        },
        b"load amnesiac\n"
    );

    assert_cmd_ok!(
        PlaylistLoad {
            playlist: "amnesiac".into(),
            range: Some(SongIndex(2)..SongIndex(5))
        },
        b"load amnesiac 2:5\n"
    );
}

#[test]
fn playlistadd() {
    assert_cmd_ok!(
        PlaylistAdd {
            playlist: "amnesiac".into(),
            song: Uri("a.m4a".into()),
        },
        b"playlistadd amnesiac a.m4a\n"
    );
}

#[test]
fn playlistclear() {
    assert_cmd_ok!(
        PlaylistClear {
            playlist: "amnesiac".into(),
        },
        b"playlistclear amnesiac\n"
    );
}

#[test]
fn playlistdelete() {
    assert_cmd_ok!(
        PlaylistDelete {
            playlist: "amnesiac".into(),
            song: SongIndex(0),
        },
        b"playlistdelete amnesiac 0\n"
    );
}

#[test]
fn playlistmove() {
    assert_cmd_ok!(
        PlaylistMove {
            playlist: "amnesiac".into(),
            from: SongIndex(0),
            to: SongIndex(1)
        },
        b"playlistmove amnesiac 0 1\n"
    );
}

#[test]
fn rename() {
    assert_cmd_ok!(
        PlaylistRename {
            from: "amnseiac".into(),
            to: "amnesiac".into(),
        },
        &b"rename amnseiac amnesiac\n"[..]
    );
}

#[test]
fn rm() {
    assert_cmd_ok!(
        PlaylistRemove {
            playlist: "amnesiac".into(),
        },
        b"rm amnesiac\n"
    );
}

#[test]
fn save() {
    assert_cmd_ok!(
        QueueSave {
            playlist: "amnesiac".into(),
        },
        b"save amnesiac\n"
    );
}
