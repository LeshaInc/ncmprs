mod common;

use ncmprs::client::types::*;
use ncmprs::client::*;

#[test]
fn add() {
    assert_cmd_ok!(QueueAdd(Uri("rip".into())), b"add rip\n");
}

#[test]
fn addid() {
    assert_cmd!(
        QueueAddId {
            uri: Uri("rip".into()),
            position: None,
        },
        b"addid rip\n",
        b"Id: 123\nOK\n",
        IdResponse { id: SongId(123) }
    );
}

#[test]
fn clear() {
    assert_cmd_ok!(QueueClear, b"clear\n");
}

#[test]
fn delete() {
    assert_cmd_ok!(QueueDelete::Single(SongIndex(1)), b"delete 1\n");
    assert_cmd_ok!(
        QueueDelete::Range(SongIndex(1)..SongIndex(2)),
        b"delete 1:2\n"
    );
}

#[test]
fn deleteid() {
    assert_cmd_ok!(QueueDeleteId(SongId(1)), b"deleteid 1\n");
}

#[test]
fn r#move() {
    assert_cmd_ok!(
        QueueMove::Single {
            from: SongIndex(1),
            to: SongIndex(3)
        },
        b"move 1 3\n"
    );

    assert_cmd_ok!(
        QueueMove::Range {
            from: SongIndex(1)..SongIndex(10),
            to: SongIndex(50)
        },
        b"move 1:10 50\n"
    );
}

#[test]
fn moveid() {
    assert_cmd_ok!(
        QueueMoveId {
            from: SongId(1),
            to: SongIndex(3)
        },
        b"moveid 1 3\n"
    );
}

#[test]
fn playlistfind() {
    assert_cmd!(
        QueueFind {
            tag: Tag::Artist,
            needle: "Radiohead".into(),
        },
        b"playlistfind artist Radiohead\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );
}

#[test]
fn playlistid() {
    assert_cmd!(
        QueueList,
        b"playlistid\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );

    assert_cmd!(
        QueueInfo(SongId(25)),
        b"playlistid 25\n",
        format!("{}\nOK\n", &*common::SONG_RES_0),
        SearchResult(vec![common::SONG_0.clone()])
    );
}

#[test]
fn playlistsearch() {
    assert_cmd!(
        QueueFind {
            tag: Tag::Artist,
            needle: "radio".into(),
        },
        b"playlistfind artist radio\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );
}

#[test]
fn plchanges() {
    assert_cmd!(
        QueueChanges {
            version: 1,
            range: None,
        },
        b"plchanges 1\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );

    assert_cmd!(
        QueueChanges {
            version: 1,
            range: Some(SongIndex(0)..SongIndex(1)),
        },
        b"plchanges 1 0:1\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );
}

#[test]
fn plchangesposid() {
    let res_b = b"cpos: 0\nId: 25\ncpos: 1\nId: 1\nOK\n";
    let res =
        QueueChangesShortResponse(vec![(SongIndex(0), SongId(25)), (SongIndex(1), SongId(1))]);

    assert_cmd!(
        QueueChangesShort {
            version: 1,
            range: None,
        },
        b"plchangesposid 1\n",
        res_b,
        res.clone()
    );

    assert_cmd!(
        QueueChangesShort {
            version: 1,
            range: Some(SongIndex(0)..SongIndex(1)),
        },
        b"plchangesposid 1 0:1\n",
        res_b,
        res.clone()
    );
}

#[test]
fn prio() {
    assert_cmd_ok!(
        SetPrio {
            prio: 255,
            songs: vec![(SongIndex(1), None), (SongIndex(5), Some(SongIndex(10)))]
        },
        b"prio 255 1 5:10\n"
    );
}

#[test]
fn prioid() {
    assert_cmd_ok!(
        SetPrioId {
            prio: 255,
            songs: vec![SongId(1), SongId(25)]
        },
        b"prioid 255 1 25\n"
    );
}

#[test]
fn rangeid() {
    assert_cmd_ok!(
        SetSongRange {
            song: SongId(1),
            range: Some(1.0)..Some(100.0),
        },
        b"rangeid 1 1:100\n"
    );

    assert_cmd_ok!(
        SetSongRange {
            song: SongId(1),
            range: Some(1.0)..None,
        },
        b"rangeid 1 1:\n"
    );

    assert_cmd_ok!(
        SetSongRange {
            song: SongId(1),
            range: None..Some(100.0),
        },
        b"rangeid 1 :100\n"
    );

    assert_cmd_ok!(
        SetSongRange {
            song: SongId(1),
            range: None..None,
        },
        b"rangeid 1 :\n"
    );
}

#[test]
fn shuffle() {
    assert_cmd_ok!(
        Shuffle {
            range: Some(SongIndex(1)..SongIndex(50)),
        },
        b"shuffle 1:50\n"
    );

    assert_cmd_ok!(Shuffle { range: None }, b"shuffle\n");
}

#[test]
fn swap() {
    assert_cmd_ok!(SwapIndex(SongIndex(13), SongIndex(37)), b"swap 13 37\n");
}

#[test]
fn swapid() {
    assert_cmd_ok!(SwapId(SongId(13), SongId(37)), b"swapid 13 37\n");
}

#[test]
fn addtagid() {
    assert_cmd_ok!(
        AddTag {
            song: SongId(1),
            tag: Tag::Artist,
            value: "Radiohead".into()
        },
        b"addtagid 1 artist Radiohead\n"
    );
}

#[test]
fn cleartagid() {
    assert_cmd_ok!(
        ClearTag {
            song: SongId(1),
            tag: Tag::Artist,
        },
        b"cleartagid 1 artist\n"
    );
}
