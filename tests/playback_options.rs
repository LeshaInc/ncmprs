mod common;

use ncmprs::client::types::*;
use ncmprs::client::*;

#[test]
fn consume() {
    assert_cmd_ok!(SetConsume(true), b"consume 1\n");
    assert_cmd_ok!(SetConsume(false), b"consume 0\n");
}

#[test]
fn crossfade() {
    assert_cmd_ok!(SetCrossfade(5), b"crossfade 5\n");
    assert_cmd_ok!(SetCrossfade(55), b"crossfade 55\n");
}

#[test]
fn mixrampdb() {
    assert_cmd_ok!(SetMixRampDB(-1.0), b"mixrampdb -1\n");
}

#[test]
fn mixrampdelay() {
    assert_cmd_ok!(SetMixRampDelay(1.0), b"mixrampdelay 1\n");
}

#[test]
fn random() {
    assert_cmd_ok!(SetRandom(true), b"random 1\n");
    assert_cmd_ok!(SetRandom(false), b"random 0\n");
}

#[test]
fn repeat() {
    assert_cmd_ok!(SetRepeat(true), b"repeat 1\n");
    assert_cmd_ok!(SetRepeat(false), b"repeat 0\n");
}

#[test]
fn setvol() {
    assert_cmd_ok!(SetVol(50), b"setvol 50\n");
}

#[test]
fn single() {
    assert_cmd_ok!(SetSingleMode(SingleMode::On), b"single 1\n");
    assert_cmd_ok!(SetSingleMode(SingleMode::Off), b"single 0\n");
    assert_cmd_ok!(SetSingleMode(SingleMode::OneShot), b"single oneshot\n");
}

#[test]
fn replay_gain_mode() {
    assert_cmd_ok!(
        SetReplayGainMode(ReplayGainMode::Off),
        b"replay_gain_mode off\n"
    );

    assert_cmd_ok!(
        SetReplayGainMode(ReplayGainMode::Track),
        b"replay_gain_mode track\n"
    );

    assert_cmd_ok!(
        SetReplayGainMode(ReplayGainMode::Album),
        b"replay_gain_mode album\n"
    );

    assert_cmd_ok!(
        SetReplayGainMode(ReplayGainMode::Auto),
        b"replay_gain_mode auto\n"
    );
}

#[test]
fn replay_gain_status() {
    assert_cmd!(
        ReplayGainStatus,
        b"replay_gain_status\n",
        b"replay_gain_mode: off\nOK\n",
        ReplayGainStatusResponse {
            mode: ReplayGainMode::Off
        }
    );
}
