use chrono::{TimeZone, Utc};
use ncmprs::client::types::*;

fn t(k: Tag, v: impl Into<String>) -> TagEntry {
    TagEntry::new(k, v.into())
}

lazy_static::lazy_static! {
    pub static ref SONG_RES_0: &'static str = indoc::indoc!(
        "file: Radiohead/Hail to the Thief/01 2 + 2 = 5.m4a
        Last-Modified: 2019-08-14T10:46:49Z
        Artist: Radiohead
        Album: Hail to the Thief
        Title: 2 + 2 = 5
        Track: 1
        Genre: Alternative
        Date: 2003-06-09
        Composer: Ed O’Brien, Philip Selway, Thom Yorke, Jonny Greenwood, Colin Greenwood
        Disc: 1
        Label: EMI Music Australia
        AlbumArtist: Radiohead
        MUSICBRAINZ_ARTISTID: a74b1b7f-71a5-4011-9441-d0b5e4122711
        MUSICBRAINZ_ALBUMID: 1fb1bcc2-1107-3b79-9fc4-db5b912651d8
        MUSICBRAINZ_ALBUMARTISTID: a74b1b7f-71a5-4011-9441-d0b5e4122711
        MUSICBRAINZ_TRACKID: 14692e9b-8a4f-40ce-b2a4-5ce1a45599bb
        MUSICBRAINZ_RELEASETRACKID: 90646650-d94f-38ee-88ba-4fba7f258664
        duration: 199.367
        Pos: 0
        Id: 1"
    );

    pub static ref TAGS_0: Vec<TagEntry> = vec![
        t(Tag::Artist, "Radiohead"),
        t(Tag::Album, "Hail to the Thief"),
        t(Tag::Title, "2 + 2 = 5"),
        t(Tag::Track, "1"),
        t(Tag::Genre, "Alternative"),
        t(Tag::Date, "2003-06-09"),
        t(Tag::Composer, "Ed O’Brien, Philip Selway, Thom Yorke, Jonny Greenwood, Colin Greenwood"),
        t(Tag::Disc, "1"),
        t(Tag::Label, "EMI Music Australia"),
        t(Tag::AlbumArtist, "Radiohead"),
        t(Tag::MusicBrainzArtistId, "a74b1b7f-71a5-4011-9441-d0b5e4122711"),
        t(Tag::MusicBrainzAlbumId, "1fb1bcc2-1107-3b79-9fc4-db5b912651d8"),
        t(Tag::MusicBrainzAlbumArtistId, "a74b1b7f-71a5-4011-9441-d0b5e4122711"),
        t(Tag::MusicBrainzTrackId, "14692e9b-8a4f-40ce-b2a4-5ce1a45599bb"),
        t(Tag::MusicBrainzReleaseTrackId, "90646650-d94f-38ee-88ba-4fba7f258664"),
    ];

    pub static ref SONG_0: SongEntry = SongEntry {
        file: "Radiohead/Hail to the Thief/01 2 + 2 = 5.m4a".into(),
        song: Song {
            artist: Some("Radiohead".into()),
            album: Some("Hail to the Thief".into()),
            title: Some("2 + 2 = 5".into()),
            track: Some(1),
            duration: 199.367,
            audio_format: None,
            last_modified: Utc.ymd(2019, 8, 14).and_hms(10, 46, 49),
            range: None..None,
            tags: TAGS_0.to_vec(),
        },
        song_index: SongIndex(0),
        song_id: SongId(1),
    };

    pub static ref SONG_RES_1: &'static str = indoc::indoc!(
        "file: Radiohead/A Moon Shaped Pool/02 Daydreaming.m4a
        Last-Modified: 2019-08-14T10:50:44Z
        Artist: Radiohead
        Album: A Moon Shaped Pool
        Title: Daydreaming
        Track: 2
        Genre: Alternative
        Date: 2016-06-17
        Disc: 1
        Label: Casete
        AlbumArtist: Radiohead
        MUSICBRAINZ_ARTISTID: a74b1b7f-71a5-4011-9441-d0b5e4122711
        MUSICBRAINZ_ALBUMID: 93bfc3e1-7fba-4e1b-88f9-8a6afadc700d
        MUSICBRAINZ_ALBUMARTISTID: a74b1b7f-71a5-4011-9441-d0b5e4122711
        MUSICBRAINZ_TRACKID: 7488e7c3-86b8-4755-a167-b053adecae13
        MUSICBRAINZ_RELEASETRACKID: c8ff0c25-2612-4dbe-82d2-274e0cc44822
        duration: 384.499
        Pos: 0
        Id: 25"
    );

    pub static ref TAGS_1: Vec<TagEntry> = vec![
        t(Tag::Artist, "Radiohead"),
        t(Tag::Album, "A Moon Shaped Pool"),
        t(Tag::Title, "Daydreaming"),
        t(Tag::Track, "2"),
        t(Tag::Genre, "Alternative"),
        t(Tag::Date, "2016-06-17"),
        t(Tag::Disc, "1"),
        t(Tag::Label, "Casete"),
        t(Tag::AlbumArtist, "Radiohead"),
        t(Tag::MusicBrainzArtistId, "a74b1b7f-71a5-4011-9441-d0b5e4122711"),
        t(Tag::MusicBrainzAlbumId, "93bfc3e1-7fba-4e1b-88f9-8a6afadc700d"),
        t(Tag::MusicBrainzAlbumArtistId, "a74b1b7f-71a5-4011-9441-d0b5e4122711"),
        t(Tag::MusicBrainzTrackId, "7488e7c3-86b8-4755-a167-b053adecae13"),
        t(Tag::MusicBrainzReleaseTrackId, "c8ff0c25-2612-4dbe-82d2-274e0cc44822"),
    ];

    pub static ref SONG_1: SongEntry = SongEntry {
        file: "Radiohead/A Moon Shaped Pool/02 Daydreaming.m4a".into(),
        song: Song {
            artist: Some("Radiohead".into()),
            album: Some("A Moon Shaped Pool".into()),
            title: Some("Daydreaming".into()),
            track: Some(2),
            duration: 384.499,
            audio_format: None,
            last_modified: Utc.ymd(2019, 8, 14).and_hms(10, 50, 44),
            range: None..None,
            tags: TAGS_1.to_vec(),
        },
        song_index: SongIndex(0),
        song_id: SongId(25),
    };

    pub static ref SONG_ENTRIES_RES: String = format!("{}\n{}\nOK\n", *SONG_RES_0, *SONG_RES_1);
    pub static ref SONG_ENTRIES: Vec<SongEntry> = vec![SONG_0.clone(), SONG_1.clone()];
    pub static ref SEARCH_RESULT: SearchResult = SearchResult(SONG_ENTRIES.clone());
}

#[macro_export]
macro_rules! assert_cmd {
    ($req:expr, $req_expected:expr, $res:expr, $res_expected:expr) => {{
        use bytes::BytesMut;
        use ncmprs::client::MpdProtocol;
        use tokio::codec::{Decoder, Encoder};

        // let len = $req.len();
        // let expected_len = $req_expected.len() - 1;
        // assert_eq!(len, expected_len);

        let mut codec = MpdProtocol::default();

        let mut dst = BytesMut::new();
        codec
            .encode($req.into(), &mut dst)
            .expect("Failed to encode command");

        assert_eq!(&dst[..], &$req_expected[..]);

        let mut src = BytesMut::from(&$res[..]);
        let cmd = codec.decode(&mut src).expect("Failed to decode command");

        assert_eq!(cmd, Some($res_expected.into()));
    }};
}

#[macro_export]
macro_rules! assert_cmd_ok {
    ($req:expr, $req_expected:expr) => {
        assert_cmd!($req, $req_expected, "OK\n", ncmprs::client::Response::Ok);
    };
}
