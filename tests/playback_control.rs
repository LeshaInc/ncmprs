mod common;

use ncmprs::client::types::*;
use ncmprs::client::*;

#[test]
fn next() {
    assert_cmd_ok!(Next, b"next\n");
}

#[test]
fn pause() {
    assert_cmd_ok!(SetPause(true), b"pause 1\n");
    assert_cmd_ok!(SetPause(false), b"pause 0\n");
}

#[test]
fn play() {
    assert_cmd_ok!(PlayIndex(SongIndex(123)), b"play 123\n");
}

#[test]
fn playid() {
    assert_cmd_ok!(PlayId(SongId(123)), b"playid 123\n");
}

#[test]
fn previous() {
    assert_cmd_ok!(Previous, b"previous\n");
}

#[test]
fn seek() {
    assert_cmd_ok!(
        SeekIndex {
            song: SongIndex(123),
            time: 12.3
        },
        b"seek 123 12.3\n"
    );
}

#[test]
fn seekid() {
    assert_cmd_ok!(
        SeekId {
            song: SongId(123),
            time: 12.3
        },
        b"seekid 123 12.3\n"
    );
}

#[test]
fn seekcur() {
    assert_cmd_ok!(SeekCur::Absolute(12.3), b"seekcur 12.3\n");
    assert_cmd_ok!(SeekCur::Relative(-12.3), b"seekcur -12.3\n");
    assert_cmd_ok!(SeekCur::Relative(12.3), b"seekcur +12.3\n");
}

#[test]
#[should_panic]
fn seekcur_negative_absolute() {
    assert_cmd_ok!(SeekCur::Absolute(-12.3), b"seekcur 12.3\n");
}

#[test]
fn stop() {
    assert_cmd_ok!(Stop, b"stop\n");
}
