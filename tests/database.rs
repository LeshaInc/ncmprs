mod common;

use ncmprs::client::types::*;
use ncmprs::client::*;

#[test]
fn albumart() {
    assert_cmd!(
        AlbumArt {
            song: Uri("Radiohead/A Moon Shaped Pool/01 Burn the Witch.m4a".into()),
            offset: 0,
        },
        b"albumart \"Radiohead/A Moon Shaped Pool/01 Burn the Witch.m4a\" 0\n",
        b"size: 1\nbinary: 1\nX\nOK\n",
        AlbumArtResponse {
            size: 1,
            chunk: b"X".to_vec()
        }
    );
}

#[test]
fn count() {
    assert_cmd!(
        Count(Filter::Tag {
            tag: Tag::Artist,
            op: FilterOperator::Regex,
            value: "^Radio".into()
        }),
        b"count \"(artist =~ '^Radio')\"\n",
        b"count: 999\nplaytime: 999999\nOK\n",
        CountResponse {
            count: 999,
            playtime: 999999
        }
    );
}

#[test]
fn count_group() {
    assert_cmd!(
        CountGroup(Filter::Tag {
            tag: Tag::Artist,
            op: FilterOperator::Regex,
            value: "^Radio".into()
        }, Tag::Album),
        b"count \"(artist =~ '^Radio')\" group album\n",
        b"album: A Moon Shaped Pool\ncount: 11\nplaytime: 999999\nalbum: In Rainbows\ncount: 10\nplaytime: 999998\nOK\n",
        CountGroupResponse {
            entries: vec![
                CountEntry {
                    headline: "A Moon Shaped Pool".into(),
                    count: 11,
                    playtime: 999999
                },
                CountEntry {
                    headline: "In Rainbows".into(),
                    count: 10,
                    playtime: 999998
                }
            ]
        }
    );
}

#[test]
fn getfingerprint() {
    assert_cmd!(
        GetFingerprint(Uri("rip".into())),
        b"getfingerprint rip\n",
        b"chromaprint: 1234\nOK\n",
        Fingerprint("1234".into())
    );
}

#[test]
fn find() {
    assert_cmd!(
        Find {
            filter: Filter::Tag {
                tag: Tag::Artist,
                op: FilterOperator::Regex,
                value: "^Radio".into()
            },
            sort: None,
            range: None
        },
        b"find \"(artist =~ '^Radio')\"\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );

    assert_cmd!(
        Find {
            filter: Filter::Tag {
                tag: Tag::Artist,
                op: FilterOperator::Regex,
                value: "^Radio".into()
            },
            sort: Some(Sort::TagAsc(Tag::Date)),
            range: None
        },
        b"find \"(artist =~ '^Radio')\" sort date\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );

    assert_cmd!(
        Find {
            filter: Filter::Tag {
                tag: Tag::Artist,
                op: FilterOperator::Regex,
                value: "^Radio".into()
            },
            sort: Some(Sort::TagAsc(Tag::Date)),
            range: Some(SongIndex(1)..SongIndex(23))
        },
        b"find \"(artist =~ '^Radio')\" sort date window 1:23\n",
        common::SONG_ENTRIES_RES,
        common::SEARCH_RESULT.clone()
    );
}
