#![deny(rust_2018_idioms)]
#![deny(unsafe_code)]

#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate newtype;
#[macro_use]
extern crate derivative;

pub mod client;
