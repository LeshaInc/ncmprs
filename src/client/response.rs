use super::types::*;
use super::*;

#[derive(Clone, Copy, Debug)]
pub enum Field<'a> {
    // key must be lowercase
    Pair { key: &'a str, value: &'a str },
    Binary(&'a [u8]),
}

impl Field<'_> {
    pub fn get(&self, kei: &str) -> Option<&str> {
        match self {
            Field::Pair { key, value } if &kei == key => Some(value),
            _ => None,
        }
    }

    pub fn get_binary(&self) -> Option<&[u8]> {
        match self {
            Field::Binary(bytes) => Some(bytes),
            _ => None,
        }
    }

    pub fn as_pair(&self) -> Option<(&str, &str)> {
        match self {
            Field::Pair { key, value } => Some((key, value)),
            _ => None,
        }
    }
}

macro_rules! define_response {
    ($( ($long:ident, $short:ident, $lower:ident),)+) => {
        #[derive(Clone, Debug, PartialEq)]
        pub enum Response {
            Ok,
            $( $short($long), )+
        }

        impl Response {
            pub fn take(&mut self) -> Response {
                std::mem::replace(self, Response::Ok)
            }

            $(
            pub fn $lower<F>(&mut self, f: F) -> &mut $long
            where
                F: FnOnce() -> $long,
            {
                match self {
                    Response::$short(ref mut inner) => inner,
                    _ => {
                        *self = Response::$short(f());
                        match self {
                            Response::$short(ref mut inner) => inner,
                            _ => unreachable!()
                        }
                    }
                }
            }
            )+
        }

        $(
        impl From<$long> for Response {
            fn from(v: $long) -> Response {
                Response::$short(v)
            }
        }
        )+
    };
}

define_response! {
    (SongEntry, SongEntry, make_song_entry),
    (SearchResult, SearchResult, make_search_result),
    (FileList, FileList, make_file_list),
    (Fingerprint, Fingerprint, make_fingerprint),
    (JobId, JobId, make_job_id),

    (IdleResponse, Idle, make_idle),
    (StatsResponse, Stats, make_stats),
    (StatusResponse, Status, make_status),

    (ReplayGainStatusResponse, ReplayGainStatus, make_replay_gain_status),

    (IdResponse, Id, make_id),
    (QueueChangesShortResponse, QueueCHangesShort, make_queue_changes_short),

    (PlaylistsListResponse, PlaylistsList, make_playlists_list),

    (AlbumArtResponse, AlbumArt, make_album_art),
    (CountResponse, Count, make_count),
    (CountGroupResponse, CountGroup, make_count_group),
    (ListResponse, List, make_list),
    (ListFilesResponse, ListFiles, make_list_files),
    (ListFilesInfoResponse, ListFilesInfo, make_list_files_info),
}

impl Default for Response {
    fn default() -> Response {
        Response::Ok
    }
}
