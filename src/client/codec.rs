use std::str;

use bytes::{BufMut, BytesMut};
use failure::Error;
use tokio::codec::{Decoder, Encoder};

use super::types::Ack;
use super::{AnyCommand, Field, Response, VisitFn};

#[derive(Default)]
pub struct MpdProtocol {
    inflight_visitors: Vec<VisitFn>,
    response: Response,
    binary_size: Option<usize>,
    colon_index: Option<usize>,
    next_index: usize,
}

impl Encoder for MpdProtocol {
    type Item = AnyCommand;
    type Error = Error;

    fn encode(&mut self, item: AnyCommand, buf: &mut BytesMut) -> Result<(), Error> {
        buf.reserve(item.len() + 1);
        item.encode(buf);
        buf.put("\n");

        self.inflight_visitors.push(item.visit_fn());
        item.begin(&mut self.response);

        Ok(())
    }
}

impl MpdProtocol {
    fn find_colon(&mut self, buf: &mut BytesMut) -> Option<usize> {
        let colon_index = if let Some(v) = self.colon_index {
            v
        } else {
            let colon_offset = match buf[self.next_index..].iter().position(|&b| b == b':') {
                Some(v) => v,
                None => {
                    self.next_index = buf.len();
                    return None;
                }
            };

            let colon_index = self.next_index + colon_offset;

            self.colon_index = Some(colon_index);
            self.next_index = colon_index;

            colon_index
        };

        Some(colon_index)
    }

    fn find_newline(&mut self, buf: &mut BytesMut) -> Option<usize> {
        match buf[self.next_index..].iter().position(|&b| b == b'\n') {
            Some(v) => Some(v + self.next_index),
            None => {
                self.next_index = buf.len();
                None
            }
        }
    }

    fn visit(&mut self, field: Field<'_>) -> Result<(), Error> {
        let visitor = match self.inflight_visitors.last() {
            Some(v) => v,
            None => return Ok(()),
        };

        visitor(field, &mut self.response)?;
        self.next_field();
        Ok(())
    }

    fn next_field(&mut self) {
        self.binary_size = None;
        self.colon_index = None;
        self.next_index = 0;
    }

    fn check_result(&mut self, buf: &mut BytesMut) -> Result<Option<Response>, Error> {
        if !buf.starts_with(b"OK") && !buf.starts_with(b"ACK") {
            return Ok(None);
        }

        let newline_index = match self.find_newline(buf) {
            Some(v) => v,
            None => return Ok(None),
        };

        let line = &buf.split_to(newline_index + 1)[..newline_index];
        let line = str::from_utf8(line)?;

        if line == "OK" {
            Ok(Some(self.response.take()))
        } else {
            let ack: Ack = line.parse()?;
            Err(ack.into())
        }
    }
}

impl Decoder for MpdProtocol {
    type Item = Response;
    type Error = Error;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Response>, Error> {
        if self.colon_index.is_none() {
            match self.check_result(buf) {
                Ok(None) => (),
                v => return v,
            }
        }

        if let Some(size) = self.binary_size {
            if buf.len() >= size {
                let data = &buf.split_to(size)[..size - 1];
                self.binary_size = None;
                self.visit(Field::Binary(data))?;
            } else {
                return Ok(None);
            }
        }

        loop {
            let colon_index = match self.find_colon(buf) {
                Some(v) => v,
                None => return Ok(None),
            };

            let newline_index = match self.find_newline(buf) {
                Some(v) => v,
                None => return Ok(None),
            };

            let mut key = buf.split_to(colon_index + 1);
            let key = str::from_utf8_mut(&mut key[..colon_index])?;
            key.make_ascii_lowercase();

            let idx = newline_index - colon_index;
            let value = buf.split_to(idx);
            let value = str::from_utf8(&value[..idx])?.trim();

            self.next_index = 0;

            if key == "binary" {
                let size = value.parse::<usize>()? + 1;
                self.binary_size = Some(size);
                buf.reserve(buf.len() - size);

                if buf.len() >= size {
                    let data = &buf.split_to(size)[..size - 1];
                    self.visit(Field::Binary(data))?;
                    self.binary_size = None;
                } else {
                    return Ok(None);
                }
            } else {
                self.visit(Field::Pair { key, value })?;
            }

            match self.check_result(buf) {
                Ok(None) => (),
                v => break v,
            }
        }
    }
}
