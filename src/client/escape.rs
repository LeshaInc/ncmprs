use std::fmt::{self, Display, Formatter};

pub trait EscapeExt {
    fn escape_single_quote(&self) -> StrEscape<'_>;
    fn escape_double_quote(&self) -> StrEscape<'_>;
    fn escape_single_quote_len(&self) -> usize;
    fn escape_double_quote_len(&self) -> usize;
}

fn should_escape(s: &str) -> bool {
    for c in s.chars() {
        if let '\n' | '\r' | '\t' | ' ' | '"' | '\'' = c {
            return true;
        }
    }

    false
}

fn escape_len(s: &str, q: char) -> usize {
    let mut len = s.len();

    if !should_escape(s) {
        return len;
    }

    for c in s.chars() {
        if let '\n' | '\r' | '\t' = c {
            len += 1;
        }

        if c == q {
            len += 1;
        }
    }

    // include quotation marks
    len + 2
}

pub struct StrEscape<'a> {
    str: &'a str,
    quote: char,
}

impl Display for StrEscape<'_> {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> fmt::Result {
        if self.quote == '"' && !should_escape(self.str) {
            return write!(fmt, "{}", self.str);
        }

        write!(fmt, "{}", self.quote)?;

        for c in self.str.chars() {
            match c {
                '\n' => write!(fmt, "\\n"),
                '\r' => write!(fmt, "\\r"),
                '\t' => write!(fmt, "\\t"),
                c if c == self.quote => write!(fmt, "\\{}", c),
                c => write!(fmt, "{}", c),
            }?;
        }

        write!(fmt, "{}", self.quote)?;

        Ok(())
    }
}

impl EscapeExt for &str {
    fn escape_single_quote(&self) -> StrEscape<'_> {
        StrEscape {
            str: self,
            quote: '\'',
        }
    }

    fn escape_double_quote(&self) -> StrEscape<'_> {
        StrEscape {
            str: self,
            quote: '"',
        }
    }

    fn escape_single_quote_len(&self) -> usize {
        escape_len(self, '\'')
    }

    fn escape_double_quote_len(&self) -> usize {
        escape_len(self, '"')
    }
}

impl EscapeExt for String {
    fn escape_single_quote(&self) -> StrEscape<'_> {
        StrEscape {
            str: self.as_str(),
            quote: '\'',
        }
    }

    fn escape_double_quote(&self) -> StrEscape<'_> {
        StrEscape {
            str: self.as_str(),
            quote: '\"',
        }
    }

    fn escape_single_quote_len(&self) -> usize {
        escape_len(self.as_str(), '\'')
    }

    fn escape_double_quote_len(&self) -> usize {
        escape_len(self.as_str(), '\"')
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn dont_escape() {
        assert_eq!("rippo".escape_double_quote().to_string(), "rippo");
        assert_eq!("rippo".escape_double_quote_len(), "rippo".len());
    }

    #[test]
    fn escape_single_quote() {
        assert_eq!(
            r#" hello "world" "#.escape_single_quote().to_string(),
            r#"' hello "world" '"#
        );

        assert_eq!(
            r#" hello "world" "#.escape_single_quote().to_string().len(),
            r#" hello "world" "#.escape_single_quote_len()
        );

        assert_eq!(
            r#" hello 'world' "#.escape_single_quote().to_string(),
            r#"' hello \'world\' '"#
        );

        assert_eq!(
            r#" hello 'world' "#.escape_single_quote().to_string().len(),
            r#" hello 'world' "#.escape_single_quote_len()
        );
    }

    #[test]
    fn escape_double_quote() {
        assert_eq!(
            r#" hello 'world' "#.escape_double_quote().to_string(),
            r#"" hello 'world' ""#
        );

        assert_eq!(
            r#" hello 'world' "#.escape_double_quote().to_string().len(),
            r#" hello 'world' "#.escape_double_quote_len()
        );

        assert_eq!(
            r#" hello "world" "#.escape_double_quote().to_string(),
            r#"" hello \"world\" ""#
        );

        assert_eq!(
            r#" hello "world" "#.escape_double_quote().to_string().len(),
            r#" hello "world" "#.escape_double_quote_len()
        );
    }
}
