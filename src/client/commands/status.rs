use bytes::{BufMut, BytesMut};
use chrono::{DateTime, TimeZone, Utc};
use failure::Fallible;

use crate::client::types::*;
use crate::client::{Command, Field, Response};

#[derive(Clone, Copy, Debug)]
pub struct ClearError;

impl Command for ClearError {
    const NAME: &'static str = "clearerror";
}

#[derive(Clone, Copy, Debug)]
pub struct CurrentSong;

impl Command for CurrentSong {
    const NAME: &'static str = "currentsong";

    fn begin(response: &mut Response) {
        response.make_song_entry(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_song_entry(Default::default).visit_field(field)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Idle(pub Subsystems);

#[derive(Clone, Debug, Default, Eq, PartialEq)]
pub struct IdleResponse {
    pub changed: Subsystems,
}

impl Command for Idle {
    const NAME: &'static str = "idle";

    fn len(&self) -> usize {
        if self.0.is_all() {
            Self::NAME.len()
        } else {
            Self::NAME.len() + 1 + self.0.len()
        }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        if !self.0.is_all() {
            buf.put(" ");
            self.0.encode(buf);
        }
    }

    fn begin(response: &mut Response) {
        response.make_idle(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_idle(Default::default);

        if let Some(v) = field.get("changed") {
            response.changed |= v.parse()?;
        }

        Ok(())
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Stats;

#[derive(Clone, Copy, Debug, PartialEq, Derivative)]
#[derivative(Default)]
pub struct StatsResponse {
    pub artists: u32,
    pub albums: u32,
    pub songs: u32,
    pub uptime: u32,
    pub db_playtime: u32,
    #[derivative(Default(value = "Utc.timestamp(0, 0)"))]
    pub db_update: DateTime<Utc>,
    pub playtime: u32,
}

impl Command for Stats {
    const NAME: &'static str = "stats";

    fn begin(response: &mut Response) {
        response.make_stats(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_stats(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "artists" => response.artists = v.parse()?,
            "albums" => response.albums = v.parse()?,
            "songs" => response.songs = v.parse()?,
            "uptime" => response.uptime = v.parse()?,
            "db_playtime" => response.db_playtime = v.parse()?,
            "db_update" => response.db_update = Utc.timestamp(v.parse()?, 0), // TODO: correct timezone
            "playtime" => response.playtime = v.parse()?,
            _ => (),
        }

        Ok(())
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Status;

#[derive(Clone, Debug, PartialEq, Default)]
pub struct StatusResponse {
    pub volume: u8,
    pub repeat: bool,
    pub random: bool,
    pub single: SingleMode,
    pub consume: bool,
    pub playlist: u32,
    pub playlist_len: u32,
    pub state: PlayerState,
    pub song_index: SongIndex,
    pub song_id: SongId,
    pub next_song_index: Option<SongIndex>,
    pub next_song_id: Option<SongId>,
    pub elapsed: f32,
    pub duration: f32,
    pub bitrate: u32,
    pub crossfade: u32,
    pub mixrampdb: f32,
    pub mixrampdelay: f32,
    pub audio_format: AudioFormat,
    pub updating_db: Option<u32>,
    pub error: Option<String>,
}

impl Command for Status {
    const NAME: &'static str = "status";

    fn begin(response: &mut Response) {
        response.make_status(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_status(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "volume" => response.volume = v.parse()?,
            "repeat" => response.repeat = parse_bool(v)?,
            "random" => response.random = parse_bool(v)?,
            "single" => response.single = v.parse()?,
            "consume" => response.consume = parse_bool(v)?,
            "playlist" => response.playlist = v.parse()?,
            "playlistlength" => response.playlist_len = v.parse()?,
            "state" => response.state = v.parse()?,
            "song" => response.song_index = v.parse()?,
            "songid" => response.song_id = v.parse()?,
            "nextsong" => response.next_song_index = Some(v.parse()?),
            "nextsongid" => response.next_song_id = Some(v.parse()?),
            "elapsed" => response.elapsed = v.parse()?,
            "duration" => response.duration = v.parse()?,
            "bitrate" => response.bitrate = v.parse()?,
            "crossfade" => response.crossfade = v.parse()?,
            "mixrampdb" => response.mixrampdb = v.parse()?,
            "mixrampdelay" => response.mixrampdelay = v.parse()?,
            "audio" => response.audio_format = v.parse()?,
            "updating_db" => response.updating_db = Some(v.parse()?),
            "error" => response.error = Some(v.to_string()),
            _ => (),
        }

        Ok(())
    }
}
