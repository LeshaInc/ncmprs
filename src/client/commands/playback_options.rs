use bytes::{BufMut, BytesMut};
use failure::Fallible;
use std::io::Write;

use crate::client::types::*;
use crate::client::{Command, Field, Response};

#[derive(Clone, Copy, Debug)]
pub struct SetConsume(pub bool);

impl Command for SetConsume {
    const NAME: &'static str = "consume";

    fn len(&self) -> usize {
        Self::NAME.len() + 2
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        buf.put(" ");
        encode_bool(self.0, buf);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetCrossfade(pub u32);

impl Command for SetCrossfade {
    const NAME: &'static str = "crossfade";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + uint_len(self.0)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetMixRampDB(pub f32);

impl Command for SetMixRampDB {
    const NAME: &'static str = "mixrampdb";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + f32_len(self.0)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetMixRampDelay(pub f32);

impl Command for SetMixRampDelay {
    const NAME: &'static str = "mixrampdelay";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + f32_len(self.0)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetRandom(pub bool);

impl Command for SetRandom {
    const NAME: &'static str = "random";

    fn len(&self) -> usize {
        Self::NAME.len() + 2
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        buf.put(" ");
        encode_bool(self.0, buf);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetRepeat(pub bool);

impl Command for SetRepeat {
    const NAME: &'static str = "repeat";

    fn len(&self) -> usize {
        Self::NAME.len() + 2
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        buf.put(" ");
        encode_bool(self.0, buf);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetVol(pub u8);

impl Command for SetVol {
    const NAME: &'static str = "setvol";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + uint_len(self.0)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetSingleMode(pub SingleMode);

impl Command for SetSingleMode {
    const NAME: &'static str = "single";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        buf.put(" ");
        buf.put(self.0.as_str());
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SetReplayGainMode(pub ReplayGainMode);

impl Command for SetReplayGainMode {
    const NAME: &'static str = "replay_gain_mode";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        buf.put(" ");
        buf.put(self.0.as_str());
    }
}

#[derive(Clone, Copy, Debug)]
pub struct ReplayGainStatus;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct ReplayGainStatusResponse {
    pub mode: ReplayGainMode,
}

impl Command for ReplayGainStatus {
    const NAME: &'static str = "replay_gain_status";

    fn begin(response: &mut Response) {
        response.make_replay_gain_status(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_replay_gain_status(Default::default);

        if let Some(v) = field.get("replay_gain_mode") {
            response.mode = v.parse()?;
        }

        Ok(())
    }
}
