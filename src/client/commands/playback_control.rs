use bytes::{BufMut, BytesMut};
use std::io::Write;

use crate::client::types::*;
use crate::client::Command;

#[derive(Clone, Copy, Debug)]
pub struct Next;

impl Command for Next {
    const NAME: &'static str = "next";
}

#[derive(Clone, Copy, Debug)]
pub struct SetPause(pub bool);

impl Command for SetPause {
    const NAME: &'static str = "pause";

    fn len(&self) -> usize {
        Self::NAME.len() + 2
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        buf.put(" ");
        encode_bool(self.0, buf);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PlayIndex(pub SongIndex);

impl Command for PlayIndex {
    const NAME: &'static str = "play";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PlayId(pub SongId);

impl Command for PlayId {
    const NAME: &'static str = "playid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Previous;

impl Command for Previous {
    const NAME: &'static str = "previous";
}

#[derive(Clone, Copy, Debug)]
pub struct SeekIndex {
    pub song: SongIndex,
    pub time: f32,
}

impl Command for SeekIndex {
    const NAME: &'static str = "seek";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.song.len() + 1 + f32_len(self.time)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} {}", self.song, self.time); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SeekId {
    pub song: SongId,
    pub time: f32,
}

impl Command for SeekId {
    const NAME: &'static str = "seekid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.song.len() + 1 + f32_len(self.time)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} {}", self.song, self.time); // BytesMut operations are infallible
    }
}

#[derive(Clone, Copy, Debug)]
pub enum SeekCur {
    Absolute(f32),
    Relative(f32),
}

impl Command for SeekCur {
    const NAME: &'static str = "seekcur";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + match self {
                SeekCur::Absolute(t) => f32_len(*t),
                SeekCur::Relative(t) => 1 + f32_len(t.abs()),
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = match self {
            SeekCur::Absolute(t) => {
                assert!(!t.is_sign_negative());
                write!(buf.writer(), " {}", t)
            }
            SeekCur::Relative(t) => {
                if t.is_sign_negative() {
                    write!(buf.writer(), " {}", t)
                } else {
                    write!(buf.writer(), " +{}", t)
                }
            }
        };
    }
}

#[derive(Clone, Copy, Debug)]
pub struct Stop;

impl Command for Stop {
    const NAME: &'static str = "stop";
}
