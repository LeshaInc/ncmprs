use std::ops::Range;

use bytes::{BufMut, BytesMut};
use failure::Fallible;
use std::io::Write;

use crate::client::types::*;
use crate::client::{Command, EscapeExt, Field, Response};

#[derive(Clone, Debug)]
pub struct QueueAdd(pub Uri);

impl Command for QueueAdd {
    const NAME: &'static str = "add";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + (self.0).0.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", (self.0).0.escape_double_quote());
    }
}

#[derive(Clone, Debug)]
pub struct QueueAddId {
    pub uri: Uri,
    pub position: Option<SongIndex>,
}

#[derive(Clone, Copy, Debug, Default, PartialEq)]
pub struct IdResponse {
    pub id: SongId,
}

impl Command for QueueAddId {
    const NAME: &'static str = "addid";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.uri.0.escape_double_quote_len()
            + match self.position {
                Some(v) => 1 + v.len(),
                None => 0,
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.uri.0.escape_double_quote());

        if let Some(SongIndex(idx)) = self.position {
            let _ = write!(buf.writer(), " {}", idx);
        }
    }

    fn begin(response: &mut Response) {
        response.make_id(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_id(Default::default);

        if let Some(v) = field.get("id") {
            response.id = v.parse()?;
        }

        Ok(())
    }
}

#[derive(Clone, Copy, Debug)]
pub struct QueueClear;

impl Command for QueueClear {
    const NAME: &'static str = "clear";
}

#[derive(Clone, Debug)]
pub enum QueueDelete {
    Single(SongIndex),
    Range(Range<SongIndex>),
}

impl Command for QueueDelete {
    const NAME: &'static str = "delete";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + match self {
                QueueDelete::Single(v) => v.len(),
                QueueDelete::Range(r) => r.start.len() + 1 + r.end.len(),
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = match self {
            QueueDelete::Single(idx) => write!(buf.writer(), " {}", idx),
            QueueDelete::Range(range) => write!(buf.writer(), " {}:{}", range.start, range.end),
        };
    }
}

#[derive(Clone, Debug)]
pub struct QueueDeleteId(pub SongId);

impl Command for QueueDeleteId {
    const NAME: &'static str = "deleteid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0);
    }
}

#[derive(Clone, Debug)]
pub enum QueueMove {
    Single {
        from: SongIndex,
        to: SongIndex,
    },
    Range {
        from: Range<SongIndex>,
        to: SongIndex,
    },
}

impl Command for QueueMove {
    const NAME: &'static str = "move";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + match self {
                QueueMove::Single { from, to } => from.len() + 1 + to.len(),
                QueueMove::Range { ref from, to } => {
                    from.start.len() + 1 + from.end.len() + 1 + to.len()
                }
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = match self {
            QueueMove::Single { from, to } => write!(buf.writer(), " {} {}", from, to),
            QueueMove::Range { from, to } => {
                write!(buf.writer(), " {}:{} {}", from.start, from.end, to)
            }
        };
    }
}

#[derive(Clone, Copy, Debug)]
pub struct QueueMoveId {
    pub from: SongId,
    // TODO: relative index
    pub to: SongIndex,
}

impl Command for QueueMoveId {
    const NAME: &'static str = "moveid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.from.len() + 1 + self.to.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} {}", self.from, self.to);
    }
}

#[derive(Clone, Debug)]
pub struct QueueFind {
    pub tag: Tag,
    pub needle: String,
}

impl Command for QueueFind {
    const NAME: &'static str = "playlistfind";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.tag.len() + 1 + self.needle.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(
            buf.writer(),
            " {} {}",
            self.tag,
            self.needle.escape_double_quote()
        );
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct QueueList;

impl Command for QueueList {
    const NAME: &'static str = "playlistid";

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct QueueInfo(pub SongId);

impl Command for QueueInfo {
    const NAME: &'static str = "playlistid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0);
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct QueueSearch {
    pub tag: Tag,
    pub needle: String,
}

impl Command for QueueSearch {
    const NAME: &'static str = "playlistsearch";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.tag.len() + 1 + self.needle.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {}",
            self.tag,
            self.needle.escape_double_quote()
        );
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct QueueChanges {
    pub version: u32,
    pub range: Option<Range<SongIndex>>,
}

impl Command for QueueChanges {
    const NAME: &'static str = "plchanges";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + uint_len(self.version)
            + match self.range {
                Some(ref v) => 1 + v.start.len() + 1 + v.end.len(),
                None => 0,
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.version);

        if let Some(ref r) = self.range {
            let _ = write!(buf.writer(), " {}:{}", r.start, r.end);
        }
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct QueueChangesShort {
    pub version: u32,
    pub range: Option<Range<SongIndex>>,
}

#[derive(Clone, Debug, Default, PartialEq, NewType)]
pub struct QueueChangesShortResponse(pub Vec<(SongIndex, SongId)>);

impl Command for QueueChangesShort {
    const NAME: &'static str = "plchangesposid";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + uint_len(self.version)
            + match self.range {
                Some(ref v) => 1 + v.start.len() + 1 + v.end.len(),
                None => 0,
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.version);

        if let Some(ref r) = self.range {
            let _ = write!(buf.writer(), " {}:{}", r.start, r.end);
        }
    }

    fn begin(response: &mut Response) {
        response.make_queue_changes_short(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let response = r.make_queue_changes_short(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "cpos" => response.0.push((v.parse()?, SongId(0))),
            "id" => {
                if let Some(last) = response.0.last_mut() {
                    last.1 = v.parse()?;
                }
            }
            _ => (),
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct SetPrio {
    pub prio: u8,
    pub songs: Vec<(SongIndex, Option<SongIndex>)>,
}

impl Command for SetPrio {
    const NAME: &'static str = "prio";

    fn len(&self) -> usize {
        let mut len = Self::NAME.len() + 1 + uint_len(self.prio);

        for (start, end) in &self.songs {
            len += 1 + start.len();

            if let Some(end) = end {
                len += 1 + end.len();
            }
        }

        len
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(buf.writer(), " {}", self.prio);

        for (start, end) in &self.songs {
            let _ = if let Some(end) = end {
                write!(buf.writer(), " {}:{}", start, end)
            } else {
                write!(buf.writer(), " {}", start)
            };
        }
    }
}

#[derive(Clone, Debug)]
pub struct SetPrioId {
    pub prio: u8,
    pub songs: Vec<SongId>,
}

impl Command for SetPrioId {
    const NAME: &'static str = "prioid";

    fn len(&self) -> usize {
        let mut len = Self::NAME.len() + 1 + uint_len(self.prio);

        for id in &self.songs {
            len += 1 + id.len();
        }

        len
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(buf.writer(), " {}", self.prio);

        for id in &self.songs {
            let _ = write!(buf.writer(), " {}", id);
        }
    }
}

#[derive(Clone, Debug)]
pub struct SetSongRange {
    pub song: SongId,
    pub range: Range<Option<f32>>,
}

impl Command for SetSongRange {
    const NAME: &'static str = "rangeid";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.song.len()
            + 1
            + self.range.start.map_or(0, f32_len)
            + 1
            + self.range.end.map_or(0, f32_len)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(buf.writer(), " {} ", self.song);

        if let Some(ref start) = self.range.start {
            let _ = write!(buf.writer(), "{}", start);
        }

        buf.put(":");

        if let Some(ref end) = self.range.end {
            let _ = write!(buf.writer(), "{}", end);
        }
    }
}

#[derive(Clone, Debug)]
pub struct Shuffle {
    pub range: Option<Range<SongIndex>>,
}

impl Command for Shuffle {
    const NAME: &'static str = "shuffle";

    fn len(&self) -> usize {
        Self::NAME.len()
            + match self.range {
                Some(ref v) => 1 + v.start.len() + 1 + v.end.len(),
                None => 0,
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        if let Some(ref range) = self.range {
            let _ = write!(buf.writer(), " {}:{}", range.start, range.end);
        }
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SwapIndex(pub SongIndex, pub SongIndex);

impl Command for SwapIndex {
    const NAME: &'static str = "swap";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len() + 1 + self.1.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} {}", self.0, self.1);
    }
}

#[derive(Clone, Copy, Debug)]
pub struct SwapId(pub SongId, pub SongId);

impl Command for SwapId {
    const NAME: &'static str = "swapid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len() + 1 + self.1.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} {}", self.0, self.1);
    }
}

#[derive(Clone, Debug)]
pub struct AddTag {
    pub song: SongId,
    pub tag: Tag,
    pub value: String,
}

impl Command for AddTag {
    const NAME: &'static str = "addtagid";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.song.len()
            + 1
            + self.tag.len()
            + 1
            + self.value.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {} {}",
            self.song,
            self.tag,
            self.value.escape_double_quote()
        );
    }
}

#[derive(Clone, Debug)]
pub struct ClearTag {
    pub song: SongId,
    pub tag: Tag,
}

impl Command for ClearTag {
    const NAME: &'static str = "cleartagid";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.song.len() + 1 + self.tag.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} {}", self.song, self.tag);
    }
}
