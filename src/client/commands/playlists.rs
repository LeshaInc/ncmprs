use std::io::Write;
use std::ops::Range;

use bytes::{BufMut, BytesMut};
use chrono::{DateTime, TimeZone, Utc};
use failure::Fallible;

use crate::client::types::*;
use crate::client::{Command, EscapeExt, Field, Response};

#[derive(Clone, Debug)]
pub struct PlaylistList {
    pub playlist: String,
}

impl Command for PlaylistList {
    const NAME: &'static str = "listplaylistinfo";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.playlist.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistListShort {
    pub playlist: String,
}

impl Command for PlaylistListShort {
    const NAME: &'static str = "listplaylist";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.playlist.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());
    }

    fn begin(response: &mut Response) {
        response.make_file_list(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_file_list(Default::default).visit_field(field)
    }
}

#[derive(Clone, Copy, Debug)]
pub struct PlaylistsList;

#[derive(Clone, Debug, PartialEq)]
pub struct PlaylistEntry {
    pub name: String,
    pub last_modified: DateTime<Utc>,
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct PlaylistsListResponse(pub Vec<PlaylistEntry>);

impl Command for PlaylistsList {
    const NAME: &'static str = "listplaylists";

    fn begin(response: &mut Response) {
        response.make_playlists_list(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let res = r.make_playlists_list(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "playlist" => res.0.push(PlaylistEntry {
                name: v.to_owned(),
                last_modified: Utc.timestamp(0, 0),
            }),

            "last-modified" => {
                if let Some(last) = res.0.last_mut() {
                    last.last_modified = DateTime::parse_from_rfc3339(v)?.with_timezone(&Utc)
                }
            }
            _ => (),
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistLoad {
    pub playlist: String,
    pub range: Option<Range<SongIndex>>,
}

impl Command for PlaylistLoad {
    const NAME: &'static str = "load";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.playlist.escape_double_quote_len()
            + match &self.range {
                Some(v) => 1 + v.start.len() + 1 + v.end.len(),
                None => 0,
            }
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());

        if let Some(range) = &self.range {
            let _ = write!(buf.writer(), " {}:{}", range.start, range.end);
        }
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistAdd {
    pub playlist: String,
    pub song: Uri,
}

impl Command for PlaylistAdd {
    const NAME: &'static str = "playlistadd";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.playlist.escape_double_quote_len()
            + 1
            + self.song.0.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {}",
            self.playlist.escape_double_quote(),
            self.song.0
        );
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistClear {
    pub playlist: String,
}

impl Command for PlaylistClear {
    const NAME: &'static str = "playlistclear";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.playlist.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistDelete {
    pub playlist: String,
    pub song: SongIndex,
}

impl Command for PlaylistDelete {
    const NAME: &'static str = "playlistdelete";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.playlist.escape_double_quote_len() + 1 + self.song.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {}",
            self.playlist.escape_double_quote(),
            self.song
        );
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistMove {
    pub playlist: String,
    pub from: SongIndex,
    pub to: SongIndex,
}

impl Command for PlaylistMove {
    const NAME: &'static str = "playlistmove";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.playlist.escape_double_quote_len()
            + 1
            + self.from.len()
            + 1
            + self.to.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {} {}",
            self.playlist.escape_double_quote(),
            self.from,
            self.to
        );
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistRename {
    pub from: String,
    pub to: String,
}

impl Command for PlaylistRename {
    const NAME: &'static str = "rename";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.from.escape_double_quote_len()
            + 1
            + self.to.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {}",
            self.from.escape_double_quote(),
            self.to.escape_double_quote()
        );
    }
}

#[derive(Clone, Debug)]
pub struct PlaylistRemove {
    pub playlist: String,
}

impl Command for PlaylistRemove {
    const NAME: &'static str = "rm";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.playlist.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());
    }
}

#[derive(Clone, Debug)]
pub struct QueueSave {
    pub playlist: String,
}

impl Command for QueueSave {
    const NAME: &'static str = "save";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.playlist.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());
    }
}
