use std::io::Write;
use std::ops::Range;

use bytes::{BufMut, BytesMut};
use chrono::{DateTime, TimeZone, Utc};
use failure::Fallible;

use crate::client::types::*;
use crate::client::{Command, EscapeExt, Field, Response};

#[derive(Clone, Debug)]
pub struct AlbumArt {
    pub song: Uri,
    pub offset: usize,
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct AlbumArtResponse {
    pub size: usize,
    pub chunk: Vec<u8>,
}

impl Command for AlbumArt {
    const NAME: &'static str = "albumart";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.song.0.escape_double_quote_len() + 1 + uint_len(self.offset)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        let _ = write!(
            buf.writer(),
            " {} {}",
            self.song.0.escape_double_quote(),
            self.offset
        );
    }

    fn begin(response: &mut Response) {
        response.make_album_art(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_album_art(Default::default);

        if let Some(v) = field.get("size") {
            response.size = v.parse()?;
        } else if let Some(v) = field.get_binary() {
            response.chunk = v.to_vec();
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct Count(pub Filter);

#[derive(Clone, Debug, Default, PartialEq)]
pub struct CountResponse {
    pub count: usize,
    pub playtime: usize,
}

impl Command for Count {
    const NAME: &'static str = "count";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.0);
    }

    fn begin(response: &mut Response) {
        response.make_count(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_count(Default::default);

        match field.as_pair() {
            Some(("count", v)) => response.count = v.parse()?,
            Some(("playtime", v)) => response.playtime = v.parse()?,
            _ => (),
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct CountGroup(pub Filter, pub Tag);

#[derive(Clone, Debug, Default, PartialEq)]
pub struct CountGroupResponse {
    pub entries: Vec<CountEntry>,
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct CountEntry {
    pub headline: String,
    pub count: usize,
    pub playtime: usize,
}

impl Command for CountGroup {
    const NAME: &'static str = "count";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.0.len() + 7 + self.1.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {} group {}", self.0, self.1);
    }

    fn begin(response: &mut Response) {
        response.make_count_group(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_count_group(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        if k.parse::<Tag>().is_ok() {
            response.entries.push(CountEntry {
                headline: v.to_owned(),
                count: 0,
                playtime: 0,
            });

            return Ok(());
        }

        if let Some(entry) = response.entries.last_mut() {
            match k {
                "count" => entry.count = v.parse()?,
                "playtime" => entry.playtime = v.parse()?,
                _ => (),
            }
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct GetFingerprint(pub Uri);

#[derive(Clone, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd, NewType)]
pub struct Fingerprint(pub String);

impl Command for GetFingerprint {
    const NAME: &'static str = "getfingerprint";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + (self.0).0.escape_double_quote_len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", (self.0).0.escape_double_quote());
    }

    fn begin(response: &mut Response) {
        response.make_fingerprint(Default::default);
    }

    fn visit_field(field: Field<'_>, response: &mut Response) -> Fallible<()> {
        let response = response.make_fingerprint(Default::default);

        if let Some(v) = field.get("chromaprint") {
            *response = Fingerprint(v.to_owned());
        }

        Ok(())
    }
}

fn params_len(filter: &Filter, sort: Option<Sort>, range: &Option<Range<SongIndex>>) -> usize {
    1 + filter.len()
        + sort.map_or(0, |s| s.len() + 6)
        + range
            .as_ref()
            .map_or(0, |r| 9 + uint_len(r.start.0) + uint_len(r.end.0))
}

fn encode_params(
    filter: &Filter,
    sort: Option<Sort>,
    range: &Option<Range<SongIndex>>,
    buf: &mut BytesMut,
) {
    let _ = write!(buf.writer(), " {}", filter);

    if let Some(sort) = sort {
        let _ = write!(buf.writer(), " sort {}", sort);
    }

    if let Some(range) = range {
        let _ = write!(buf.writer(), " window {}:{}", range.start, range.end);
    }
}

#[derive(Clone, Debug)]
pub struct Find {
    pub filter: Filter,
    pub sort: Option<Sort>,
    pub range: Option<Range<SongIndex>>,
}

impl Command for Find {
    const NAME: &'static str = "find";

    fn len(&self) -> usize {
        Self::NAME.len() + params_len(&self.filter, self.sort, &self.range)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        encode_params(&self.filter, self.sort, &self.range, buf);
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct FindAdd {
    pub filter: Filter,
    pub sort: Option<Sort>,
    pub range: Option<Range<SongIndex>>,
}

impl Command for FindAdd {
    const NAME: &'static str = "findadd";

    fn len(&self) -> usize {
        Self::NAME.len() + params_len(&self.filter, self.sort, &self.range)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        encode_params(&self.filter, self.sort, &self.range, buf);
    }
}

#[derive(Clone, Debug)]
pub struct Search {
    pub filter: Filter,
    pub sort: Option<Sort>,
    pub range: Option<Range<SongIndex>>,
}

impl Command for Search {
    const NAME: &'static str = "search";

    fn len(&self) -> usize {
        Self::NAME.len() + params_len(&self.filter, self.sort, &self.range)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        encode_params(&self.filter, self.sort, &self.range, buf);
    }

    fn begin(response: &mut Response) {
        response.make_search_result(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        r.make_search_result(Default::default).visit_field(field)
    }
}

#[derive(Clone, Debug)]
pub struct SearchAdd {
    pub filter: Filter,
    pub sort: Option<Sort>,
    pub range: Option<Range<SongIndex>>,
}

impl Command for SearchAdd {
    const NAME: &'static str = "searchadd";

    fn len(&self) -> usize {
        Self::NAME.len() + params_len(&self.filter, self.sort, &self.range)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        encode_params(&self.filter, self.sort, &self.range, buf);
    }
}

#[derive(Clone, Debug)]
pub struct SearchAddPlaylist {
    pub playlist: String,
    pub filter: Filter,
    pub sort: Option<Sort>,
    pub range: Option<Range<SongIndex>>,
}

impl Command for SearchAddPlaylist {
    const NAME: &'static str = "searchaddpl";

    fn len(&self) -> usize {
        Self::NAME.len()
            + 1
            + self.playlist.escape_double_quote_len()
            + params_len(&self.filter, self.sort, &self.range)
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.playlist.escape_double_quote());
        encode_params(&self.filter, self.sort, &self.range, buf);
    }
}

// TODO: grouping
#[derive(Clone, Debug)]
pub struct List {
    pub tag: Tag,
    pub filter: Option<Filter>,
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct ListResponse {
    pub entries: Vec<String>,
}

impl Command for List {
    const NAME: &'static str = "list";

    fn len(&self) -> usize {
        Self::NAME.len() + 1 + self.tag.len() + self.filter.as_ref().map_or(0, |f| 1 + f.len())
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
        let _ = write!(buf.writer(), " {}", self.tag);

        if let Some(filter) = &self.filter {
            let _ = write!(buf.writer(), " {}", filter);
        }
    }

    fn begin(response: &mut Response) {
        response.make_list(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let response = r.make_list(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        if k.parse::<Tag>().is_ok() {
            response.entries.push(v.to_owned());
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct ListFiles {
    pub directory: Option<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum FileEntry {
    Directory {
        name: String,
        last_modified: DateTime<Utc>,
    },
    File {
        name: String,
        size: usize,
        last_modified: DateTime<Utc>,
    },
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct ListFilesResponse {
    pub entries: Vec<FileEntry>,
}

impl Command for ListFiles {
    const NAME: &'static str = "list";

    fn len(&self) -> usize {
        Self::NAME.len()
            + self
                .directory
                .as_ref()
                .map_or(0, |d| 1 + d.escape_double_quote_len())
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        if let Some(d) = &self.directory {
            let _ = write!(buf.writer(), " {}", d.escape_double_quote());
        }
    }

    fn begin(response: &mut Response) {
        response.make_list_files(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let response = r.make_list_files(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "directory" => response.entries.push(FileEntry::Directory {
                name: v.to_owned(),
                last_modified: Utc.timestamp(0, 0),
            }),

            "file" => response.entries.push(FileEntry::File {
                name: v.to_owned(),
                size: 0,
                last_modified: Utc.timestamp(0, 0),
            }),

            "size" => {
                if let Some(FileEntry::File { ref mut size, .. }) = response.entries.last_mut() {
                    *size = v.parse()?;
                }
            }

            "last-modified" => {
                let v = DateTime::parse_from_rfc3339(v)?.with_timezone(&Utc);

                match response.entries.last_mut() {
                    Some(FileEntry::File {
                        ref mut last_modified,
                        ..
                    }) => *last_modified = v,

                    Some(FileEntry::Directory {
                        ref mut last_modified,
                        ..
                    }) => *last_modified = v,

                    _ => (),
                }
            }

            _ => (),
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct ListFilesInfo {
    pub directory: Option<String>,
}

#[derive(Clone, Debug, PartialEq)]
pub enum FileEntryInfo {
    Directory {
        name: String,
        last_modified: DateTime<Utc>,
    },
    File {
        name: String,
        song: Song,
    },
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct ListFilesInfoResponse {
    pub entries: Vec<FileEntryInfo>,
}

impl Command for ListFilesInfo {
    const NAME: &'static str = "lsinfo";

    fn len(&self) -> usize {
        Self::NAME.len()
            + self
                .directory
                .as_ref()
                .map_or(0, |d| 1 + d.escape_double_quote_len())
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);

        if let Some(d) = &self.directory {
            let _ = write!(buf.writer(), " {}", d.escape_double_quote());
        }
    }

    fn begin(response: &mut Response) {
        response.make_list_files_info(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let response = r.make_list_files_info(Default::default);

        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "directory" => response.entries.push(FileEntryInfo::Directory {
                name: v.to_owned(),
                last_modified: Utc.timestamp(0, 0),
            }),

            "file" => response.entries.push(FileEntryInfo::File {
                name: v.to_owned(),
                song: Song::default(),
            }),

            "last-modified" => {
                let v = DateTime::parse_from_rfc3339(v)?.with_timezone(&Utc);

                match response.entries.last_mut() {
                    Some(FileEntryInfo::File { ref mut song, .. }) => song.last_modified = v,

                    Some(FileEntryInfo::Directory {
                        ref mut last_modified,
                        ..
                    }) => *last_modified = v,

                    _ => (),
                }
            }

            _ => {
                if let Some(FileEntryInfo::File { ref mut song, .. }) = response.entries.last_mut()
                {
                    song.visit_field(field)?;
                }
            }
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct UpdateDb;

#[derive(Clone, Debug, Default, PartialEq)]
pub struct JobId(pub usize);

impl Command for UpdateDb {
    const NAME: &'static str = "update";

    fn begin(response: &mut Response) {
        response.make_job_id(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let response = r.make_job_id(Default::default);

        if let Some(v) = field.get("updating_db") {
            response.0 = v.parse()?;
        }

        Ok(())
    }
}

#[derive(Clone, Debug)]
pub struct RescanDb;

impl Command for RescanDb {
    const NAME: &'static str = "rescan";

    fn begin(response: &mut Response) {
        response.make_job_id(Default::default);
    }

    fn visit_field(field: Field<'_>, r: &mut Response) -> Fallible<()> {
        let response = r.make_job_id(Default::default);

        if let Some(v) = field.get("updating_db") {
            response.0 = v.parse()?;
        }

        Ok(())
    }
}
