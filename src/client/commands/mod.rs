mod database;
mod playback_control;
mod playback_options;
mod playlists;
mod queue;
mod status;

pub use self::database::*;
pub use self::playback_control::*;
pub use self::playback_options::*;
pub use self::playlists::*;
pub use self::queue::*;
pub use self::status::*;

use bytes::{BufMut, BytesMut};
use failure::Fallible;

use super::response::{Field, Response};

pub trait Command {
    const NAME: &'static str;
    const FIELDS: &'static [&'static str] = &[];

    fn len(&self) -> usize {
        Self::NAME.len()
    }

    fn encode(&self, buf: &mut BytesMut) {
        self.encode_name(buf);
    }

    fn encode_name(&self, buf: &mut BytesMut) {
        buf.put(Self::NAME);
    }

    fn begin(_response: &mut Response) {}

    fn visit_field(_field: Field<'_>, _response: &mut Response) -> Fallible<()> {
        Ok(())
    }
}

pub type VisitFn = fn(Field<'_>, &mut Response) -> Fallible<()>;

macro_rules! define_any_command {
    ($( $name:ident, )+) => {
        pub enum AnyCommand {
            $( $name($name), )+
        }

        impl AnyCommand {
            pub fn len(&self) -> usize {
                match self {
                    $( AnyCommand::$name(inner) => inner.len(), )+
                }
            }

            pub fn encode(&self, buf: &mut BytesMut) {
                match self {
                    $( AnyCommand::$name(inner) => inner.encode(buf), )+
                }
            }

            pub fn visit_fn(&self) -> VisitFn {
                match self {
                    $( AnyCommand::$name(_) => <$name as Command>::visit_field, )+
                }
            }

            pub fn begin(&self, res: &mut Response) {
                match self {
                    $( AnyCommand::$name(_) => <$name as Command>::begin(res), )+
                }
            }
        }

        $(
        impl From<$name> for AnyCommand {
            fn from(v: $name) -> AnyCommand {
                AnyCommand::$name(v)
            }
        }
        )+
    }
}

define_any_command! {
    // status
    ClearError, CurrentSong, Idle, Status, Stats,
    // playback options
    SetConsume, SetCrossfade, SetMixRampDB, SetMixRampDelay, SetRandom, SetRepeat, SetVol, SetSingleMode,
    SetReplayGainMode, ReplayGainStatus,
    // playback control
    Next, SetPause, PlayIndex, PlayId, Previous, SeekIndex, SeekId, SeekCur, Stop,
    // queue
    QueueAdd, QueueAddId, QueueClear, QueueDelete, QueueDeleteId, QueueMove, QueueMoveId, QueueFind, QueueList,
    QueueInfo, QueueSearch, QueueChanges, QueueChangesShort, SetPrio, SetPrioId, SetSongRange, Shuffle, SwapIndex,
    SwapId, AddTag, ClearTag,
    // playlists
    PlaylistList, PlaylistListShort, PlaylistsList, PlaylistLoad, PlaylistAdd, PlaylistClear,
    PlaylistDelete, PlaylistMove, PlaylistRename, PlaylistRemove, QueueSave,
    // database
    AlbumArt, Count, CountGroup, GetFingerprint, Find, FindAdd, Search, SearchAdd, SearchAddPlaylist, List, ListFiles,
    ListFilesInfo, UpdateDb, RescanDb,
}
