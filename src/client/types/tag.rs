use std::fmt::{self, Display, Formatter};
use std::str::FromStr;

use failure::Fail;

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Tag {
    Artist,
    ArtistSort,
    Album,
    AlbumSort,
    AlbumArtist,
    AlbumArtistSort,
    Title,
    Track,
    Name,
    Genre,
    Date,
    Composer,
    Performer,
    Work,
    Grouping,
    Comment,
    Disc,
    Label,
    MusicBrainzArtistId,
    MusicBrainzAlbumId,
    MusicBrainzAlbumArtistId,
    MusicBrainzTrackId,
    MusicBrainzReleaseTrackId,
    MusicBrainzWorkId,
}

impl Tag {
    pub fn as_str(self) -> &'static str {
        use Tag::*;

        match self {
            Artist => "artist",
            ArtistSort => "artistsort",
            Album => "album",
            AlbumSort => "albumsort",
            AlbumArtist => "albumartist",
            AlbumArtistSort => "albumartistsort",
            Title => "title",
            Track => "track",
            Name => "name",
            Genre => "genre",
            Date => "date",
            Composer => "composer",
            Performer => "performer",
            Work => "work",
            Grouping => "grouping",
            Comment => "comment",
            Disc => "disc",
            Label => "label",
            MusicBrainzArtistId => "musicbrainz_artistid",
            MusicBrainzAlbumId => "musicbrainz_albumid",
            MusicBrainzAlbumArtistId => "musicbrainz_albumartistid",
            MusicBrainzTrackId => "musicbrainz_trackid",
            MusicBrainzReleaseTrackId => "musicbrainz_releasetrackid",
            MusicBrainzWorkId => "musicbrainz_workid",
        }
    }

    pub fn len(self) -> usize {
        self.as_str().len()
    }
}

impl Display for Tag {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.pad(self.as_str())
    }
}

#[derive(Debug, Fail)]
#[fail(display = "invalid tag name")]
pub struct InvalidTag;

impl FromStr for Tag {
    type Err = InvalidTag;

    fn from_str(s: &str) -> Result<Tag, InvalidTag> {
        use Tag::*;

        let tag = match s {
            "artist" => Artist,
            "artistsort" => ArtistSort,
            "album" => Album,
            "albumsort" => AlbumSort,
            "albumartist" => AlbumArtist,
            "albumartistsort" => AlbumArtistSort,
            "title" => Title,
            "track" => Track,
            "name" => Name,
            "genre" => Genre,
            "date" => Date,
            "composer" => Composer,
            "performer" => Performer,
            "work" => Work,
            "grouping" => Grouping,
            "comment" => Comment,
            "disc" => Disc,
            "label" => Label,
            "musicbrainz_artistid" => MusicBrainzArtistId,
            "musicbrainz_albumid" => MusicBrainzAlbumId,
            "musicbrainz_albumartistid" => MusicBrainzAlbumArtistId,
            "musicbrainz_trackid" => MusicBrainzTrackId,
            "musicbrainz_releasetrackid" => MusicBrainzReleaseTrackId,
            "musicbrainz_workid" => MusicBrainzWorkId,
            _ => return Err(InvalidTag),
        };

        Ok(tag)
    }
}
