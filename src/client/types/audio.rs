use std::fmt::{self, Display, Formatter};
use std::num::ParseIntError;
use std::str::FromStr;

use failure::Fail;

use super::{uint_len, InvalidVariant};

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, Derivative)]
#[derivative(Default)]
pub enum AudioBits {
    #[derivative(Default)]
    Int8,
    Int16,
    Int24,
    Int32,
    Float32,
    // TODO: DSD
}

impl AudioBits {
    pub fn as_str(&self) -> &'static str {
        match self {
            AudioBits::Int8 => "8",
            AudioBits::Int16 => "16",
            AudioBits::Int24 => "24",
            AudioBits::Int32 => "32",
            AudioBits::Float32 => "f",
        }
    }

    pub fn len(&self) -> usize {
        self.as_str().len()
    }
}

impl FromStr for AudioBits {
    type Err = InvalidVariant;

    fn from_str(s: &str) -> Result<AudioBits, InvalidVariant> {
        match s {
            "8" => Ok(AudioBits::Int8),
            "16" => Ok(AudioBits::Int16),
            "24" => Ok(AudioBits::Int24),
            "32" => Ok(AudioBits::Int32),
            "f" => Ok(AudioBits::Float32),
            _ => Err(InvalidVariant("audio format `bits` field")),
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct AudioFormat {
    pub sample_rate: u32,
    pub bits: AudioBits,
    pub channels: u8,
}

impl AudioFormat {
    pub fn len(&self) -> usize {
        uint_len(self.sample_rate) + 1 + self.bits.len() + 1 + uint_len(self.channels)
    }
}

#[derive(Debug, Fail)]
pub enum ParseAudioFormatError {
    #[fail(display = "invalid audio format: {}", _0)]
    ParseIntError(#[cause] ParseIntError),

    #[fail(display = "invalid audio format: missing parts")]
    MissingParts,

    #[fail(display = "invalid audio format: trailing characters")]
    TrailingCharacters,

    #[fail(display = "invalid audio format: {}", _0)]
    InvalidVariant(#[cause] InvalidVariant),
}

impl From<ParseIntError> for ParseAudioFormatError {
    fn from(i: ParseIntError) -> ParseAudioFormatError {
        ParseAudioFormatError::ParseIntError(i)
    }
}

impl From<InvalidVariant> for ParseAudioFormatError {
    fn from(i: InvalidVariant) -> ParseAudioFormatError {
        ParseAudioFormatError::InvalidVariant(i)
    }
}

impl FromStr for AudioFormat {
    type Err = ParseAudioFormatError;

    fn from_str(s: &str) -> Result<AudioFormat, ParseAudioFormatError> {
        use ParseAudioFormatError::*;

        let mut split = s.split(':');

        let audio = AudioFormat {
            sample_rate: split.next().ok_or(MissingParts)?.parse()?,
            bits: split.next().ok_or(MissingParts)?.parse()?,
            channels: split.next().ok_or(MissingParts)?.parse()?,
        };

        if split.next().is_some() {
            Err(TrailingCharacters)
        } else {
            Ok(audio)
        }
    }
}

impl Display for AudioFormat {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}:{}:{}",
            self.sample_rate,
            self.bits.as_str(),
            self.channels
        )
    }
}
