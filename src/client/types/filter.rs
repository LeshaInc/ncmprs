use std::fmt::{self, Display, Formatter};

use super::{AudioFormat, EscapeExt, Tag};

#[derive(Copy, Clone, Debug)]
pub enum FilterOperator {
    Equal,
    NotEqual,
    Contains,
    Regex,
    NotRegex,
}

impl FilterOperator {
    pub fn as_str(&self) -> &'static str {
        match self {
            FilterOperator::Equal => "==",
            FilterOperator::NotEqual => "!=",
            FilterOperator::Contains => "contains",
            FilterOperator::Regex => "=~",
            FilterOperator::NotRegex => "!~",
        }
    }

    pub fn len(&self) -> usize {
        self.as_str().len()
    }
}

#[derive(Clone, Debug)]
pub enum Filter {
    Tag {
        tag: Tag,
        op: FilterOperator,
        value: String,
    },
    Uri {
        op: FilterOperator,
        value: String,
    },
    Base(String),
    AudioFormat(AudioFormat),
    // TODO: AudioFormatMask (AudioFormat =~ 'SAMPLERATE:BITS:CHANNELS')
    Not(Box<Filter>),
    And(Box<Filter>, Box<Filter>),
    Or(Box<Filter>, Box<Filter>),
}

impl Filter {
    pub fn len(&self) -> usize {
        self._fmt().to_string().escape_double_quote_len()
    }

    fn _fmt(&self) -> FmtInner<'_> {
        FmtInner(self)
    }
}

struct FmtInner<'a>(&'a Filter);

impl Display for FmtInner<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self.0 {
            Filter::Tag { tag, op, ref value } => write!(
                f,
                "({} {} {})",
                tag,
                op.as_str(),
                value.escape_single_quote()
            ),

            Filter::Uri { op, ref value } => {
                write!(f, "(file {} {})", op.as_str(), value.escape_single_quote())
            }

            Filter::Base(ref base) => write!(f, "(base {})", base.escape_single_quote()),
            Filter::AudioFormat(format) => write!(f, "(AudioFormat {})", format),
            Filter::Not(ref expr) => write!(f, "(!{})", expr._fmt()),
            Filter::And(ref lhs, ref rhs) => write!(f, "({} AND {})", lhs._fmt(), rhs._fmt()),
            Filter::Or(ref lhs, ref rhs) => {
                write!(f, "(!((!{}) AND (!{})))", lhs._fmt(), rhs._fmt())
            }
        }
    }
}

impl Display for Filter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self._fmt().to_string().escape_double_quote().fmt(f)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub enum Sort {
    TagAsc(Tag),
    TagDesc(Tag),
    LastModAsc,
    LastModDesc,
}

impl Sort {
    pub fn len(&self) -> usize {
        match self {
            Sort::TagAsc(tag) => tag.len(),
            Sort::TagDesc(tag) => tag.len() + 1,
            Sort::LastModAsc => "Last-Modified".len(),
            Sort::LastModDesc => "Last-Modified".len() + 1,
        }
    }
}

impl Display for Sort {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        match self {
            Sort::TagAsc(tag) => write!(f, "{}", tag),
            Sort::TagDesc(tag) => write!(f, "-{}", tag),
            Sort::LastModAsc => write!(f, "Last-Modified"),
            Sort::LastModDesc => write!(f, "-Last-Modified"),
        }
    }
}
