mod ack;
mod audio;
mod filter;
mod song;
mod subsystems;
mod tag;

pub use self::ack::*;
pub use self::audio::*;
pub use self::filter::*;
pub use self::song::*;
pub use self::subsystems::*;
pub use self::tag::*;

use std::fmt::{self, Display, Formatter};
use std::num::ParseIntError;
use std::ops::DivAssign;
use std::str::FromStr;

use bytes::{BufMut, BytesMut};
use failure::Fail;

use super::EscapeExt;

#[derive(Debug, Fail)]
#[fail(display = "invalid {} variant", _0)]
pub struct InvalidVariant(&'static str);

pub fn parse_bool(s: &str) -> Result<bool, InvalidVariant> {
    match s {
        "0" => Ok(false),
        "1" => Ok(true),
        _ => Err(InvalidVariant("bool")),
    }
}

pub fn encode_bool(val: bool, buf: &mut BytesMut) {
    buf.reserve(1);

    if val {
        buf.put("1")
    } else {
        buf.put("0")
    }
}

pub fn uint_len<T: DivAssign + PartialOrd + From<u8>>(mut value: T) -> usize {
    let mut len = 1;

    while value > 9.into() {
        len += 1;
        value /= 10.into();
    }

    len
}

pub fn f32_len(value: f32) -> usize {
    // FIXME: unnecessary allocation
    value.to_string().len()
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, Derivative)]
#[derivative(Default)]
pub enum SingleMode {
    #[derivative(Default)]
    Off,
    On,
    OneShot,
}

impl SingleMode {
    pub fn as_str(self) -> &'static str {
        match self {
            SingleMode::Off => "0",
            SingleMode::On => "1",
            SingleMode::OneShot => "oneshot",
        }
    }

    pub fn len(self) -> usize {
        self.as_str().len()
    }
}

impl FromStr for SingleMode {
    type Err = InvalidVariant;

    fn from_str(s: &str) -> Result<SingleMode, InvalidVariant> {
        match s {
            "0" => Ok(SingleMode::Off),
            "1" => Ok(SingleMode::On),
            "oneshot" => Ok(SingleMode::OneShot),
            _ => Err(InvalidVariant("single mode")),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, Derivative)]
#[derivative(Default)]
pub enum PlayerState {
    Play,
    #[derivative(Default)]
    Stop,
    Pause,
}

impl PlayerState {
    pub fn as_str(self) -> &'static str {
        match self {
            PlayerState::Play => "play",
            PlayerState::Stop => "stop",
            PlayerState::Pause => "pause",
        }
    }

    pub fn len(self) -> usize {
        self.as_str().len()
    }
}

impl FromStr for PlayerState {
    type Err = InvalidVariant;

    fn from_str(s: &str) -> Result<PlayerState, InvalidVariant> {
        match s {
            "play" => Ok(PlayerState::Play),
            "stop" => Ok(PlayerState::Stop),
            "pause" => Ok(PlayerState::Pause),
            _ => Err(InvalidVariant("player state")),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, Ord, PartialEq, PartialOrd, Derivative)]
#[derivative(Default)]
pub enum ReplayGainMode {
    #[derivative(Default)]
    Off,
    Track,
    Album,
    Auto,
}

impl ReplayGainMode {
    pub fn as_str(self) -> &'static str {
        match self {
            ReplayGainMode::Off => "off",
            ReplayGainMode::Track => "track",
            ReplayGainMode::Album => "album",
            ReplayGainMode::Auto => "auto",
        }
    }

    pub fn len(self) -> usize {
        self.as_str().len()
    }
}

impl FromStr for ReplayGainMode {
    type Err = InvalidVariant;

    fn from_str(s: &str) -> Result<ReplayGainMode, InvalidVariant> {
        match s {
            "off" => Ok(ReplayGainMode::Off),
            "track" => Ok(ReplayGainMode::Track),
            "album" => Ok(ReplayGainMode::Album),
            "auto" => Ok(ReplayGainMode::Auto),
            _ => Err(InvalidVariant("replay gain mode")),
        }
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd, NewType)]
#[repr(transparent)]
pub struct SongIndex(pub u32);

impl SongIndex {
    pub fn len(self) -> usize {
        uint_len(self.0)
    }
}

impl FromStr for SongIndex {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse().map(SongIndex)
    }
}

impl Display for SongIndex {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Clone, Copy, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd, NewType)]
#[repr(transparent)]
pub struct SongId(pub u32);

impl SongId {
    pub fn len(self) -> usize {
        uint_len(self.0)
    }
}

impl FromStr for SongId {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        s.parse().map(SongId)
    }
}

impl Display for SongId {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

#[derive(Clone, Debug, Default, Eq, Hash, Ord, PartialEq, PartialOrd, NewType)]
pub struct Uri(pub String);
