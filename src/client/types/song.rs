use std::ops::Range;

use chrono::{DateTime, TimeZone, Utc};
use failure::Fallible;

use super::{AudioFormat, SongId, SongIndex, Tag};
use crate::client::Field;

fn parse_song_range(s: &str) -> Range<Option<f32>> {
    let mut split = s.split('-');

    let start = split.next().and_then(|v| v.parse().ok());
    let end = split.next().and_then(|v| v.parse().ok());

    start..end
}

#[derive(Clone, Debug, PartialEq, Derivative)]
#[derivative(Default)]
pub struct Song {
    pub duration: f32,
    #[derivative(Default(value = "None..None"))]
    pub range: Range<Option<f32>>,
    pub audio_format: Option<AudioFormat>,
    #[derivative(Default(value = "Utc.timestamp(0, 0)"))]
    pub last_modified: DateTime<Utc>,
    pub tags: Vec<TagEntry>,
    pub artist: Option<String>,
    pub album: Option<String>,
    pub title: Option<String>,
    pub track: Option<u32>,
}

impl Song {
    fn add_tag(&mut self, tag: Tag, value: String) {
        match tag {
            Tag::Artist => self.artist = Some(value.clone()),
            Tag::Album => self.album = Some(value.clone()),
            Tag::Title => self.title = Some(value.clone()),
            Tag::Track => self.track = value.parse().ok(),
            _ => (),
        }

        self.tags.push(TagEntry { tag, value });
    }

    pub fn visit_field(&mut self, field: Field<'_>) -> Fallible<()> {
        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "duration" => self.duration = v.parse()?,
            "format" => self.audio_format = Some(v.parse()?),
            "range" => self.range = parse_song_range(v),
            "last-modified" => {
                self.last_modified = DateTime::parse_from_rfc3339(v)?.with_timezone(&Utc)
            } // TODO: correct timezone,
            s => {
                if let Ok(tag) = s.parse() {
                    self.add_tag(tag, v.to_string())
                }
            }
        }

        Ok(())
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TagEntry {
    pub tag: Tag,
    pub value: String,
}

impl TagEntry {
    pub fn new(tag: Tag, value: String) -> TagEntry {
        TagEntry { tag, value }
    }
}

#[derive(Clone, Debug, Default, PartialEq)]
pub struct SongEntry {
    pub file: String,
    pub song: Song,
    pub song_index: SongIndex,
    pub song_id: SongId,
}

impl SongEntry {
    pub fn visit_field(&mut self, field: Field<'_>) -> Fallible<()> {
        let (k, v) = match field.as_pair() {
            Some(v) => v,
            None => return Ok(()),
        };

        match k {
            "file" => self.file = v.to_owned(),
            "pos" => self.song_index = v.parse()?,
            "id" => self.song_id = v.parse()?,
            _ => return self.song.visit_field(field),
        }

        Ok(())
    }
}

#[derive(Clone, Debug, Default, PartialEq, NewType)]
pub struct SearchResult(pub Vec<SongEntry>);

impl SearchResult {
    pub fn visit_field(&mut self, field: Field<'_>) -> Fallible<()> {
        if let Some(("file", v)) = field.as_pair() {
            self.0.push(SongEntry {
                file: v.to_owned(),
                ..SongEntry::default()
            });
        } else if let Some(last) = self.0.last_mut() {
            last.visit_field(field)?;
        }

        Ok(())
    }
}

#[derive(Clone, Debug, Default, PartialEq, NewType)]
pub struct FileList(pub Vec<String>);

impl FileList {
    pub fn visit_field(&mut self, field: Field<'_>) -> Fallible<()> {
        if let Some(("file", v)) = field.as_pair() {
            self.0.push(v.to_owned());
        }

        Ok(())
    }
}
