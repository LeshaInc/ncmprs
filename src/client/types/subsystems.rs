use super::InvalidVariant;
use bytes::{BufMut, BytesMut};
use std::str::FromStr;

macro_rules! define_subsystems {
    ($( $name:ident => ($bit:literal, $str:literal) ),+) => {
        bitflags! {
            #[derive(Default)]
            pub struct Subsystems: u16 {
                $( const $name = 1 << $bit; )+
            }
        }

        impl Subsystems {
            #[allow(unused_assignments)]
            pub fn len(&self) -> usize {
                let mut is_first = true;
                let mut len = 0;

                $(
                if self.contains(Self::$name) {
                    if !is_first {
                        len += 1;
                    }

                    len += $str.len();
                    is_first = false;
                }
                )+

                len
            }

            #[allow(unused_assignments)]
            pub fn encode(&self, buf: &mut BytesMut) {
                let mut is_first = true;

                $(
                if self.contains(Self::$name) {
                    if !is_first {
                        buf.put(" ");
                    }

                    buf.put($str);
                    is_first = false;
                }
                )+
            }
        }

        impl FromStr for Subsystems {
            type Err = InvalidVariant;

            fn from_str(s: &str) -> Result<Subsystems, InvalidVariant> {
                match s {
                    $( $str => Ok(Self::$name), )+
                    _ => Err(InvalidVariant("subsystem")),
                }
            }
        }
    }
}

define_subsystems! {
    DATABASE => (0, "database"),
    UPDATE => (1, "update"),
    STORED_PLAYLIST => (2, "stored_playlist"),
    PLAYLIST => (3, "playlist"),
    PLAYER => (4, "player"),
    MIXER => (5, "mixer"),
    OUTPUT => (6, "output"),
    OPTIONS => (7, "options"),
    PARTITION => (8, "partition"),
    STICKER => (9, "sticker"),
    SUBSCRIPTION => (10, "subscription"),
    MESSAGE => (11, "message")
}
