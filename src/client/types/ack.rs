use std::fmt::{self, Display, Formatter};
use std::num::ParseIntError;
use std::str::FromStr;

use failure::Fail;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum ErrorCode {
    NotList = 1,
    Arg = 2,
    Password = 3,
    Permission = 4,
    Unknown = 5,

    NoExist = 50,
    PlaylistMax = 51,
    System = 52,
    PlaylistLoad = 53,
    UpdateAlready = 54,
    PlayerSync = 55,
    Exist = 56,

    Invalid = 255,
}

impl Display for ErrorCode {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", *self as u8)
    }
}

impl From<u8> for ErrorCode {
    fn from(v: u8) -> ErrorCode {
        match v {
            1 => ErrorCode::NotList,
            2 => ErrorCode::Arg,
            3 => ErrorCode::Password,
            4 => ErrorCode::Permission,
            5 => ErrorCode::Unknown,

            50 => ErrorCode::NoExist,
            51 => ErrorCode::PlaylistMax,
            52 => ErrorCode::System,
            53 => ErrorCode::PlaylistLoad,
            54 => ErrorCode::UpdateAlready,
            55 => ErrorCode::PlayerSync,
            56 => ErrorCode::Exist,

            _ => ErrorCode::Invalid,
        }
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq, Fail)]
#[fail(
    display = "ACK [{}@{}] {{{}}} {}",
    error_code, command_index, current_command, message
)]
pub struct Ack {
    pub error_code: ErrorCode,
    pub command_index: u16,
    pub current_command: String,
    pub message: String,
}

#[derive(Debug, Fail)]
pub enum ParseAckError {
    #[fail(display = "invalid ack: {}", _0)]
    ParseIntError(#[cause] ParseIntError),

    #[fail(display = "invalid ack: missing parts")]
    MissingParts,

    #[fail(display = "invalid ack: trailing characters")]
    TrailingCharacters,
}

impl From<ParseIntError> for ParseAckError {
    fn from(i: ParseIntError) -> ParseAckError {
        ParseAckError::ParseIntError(i)
    }
}

impl FromStr for Ack {
    type Err = ParseAckError;

    fn from_str(s: &str) -> Result<Ack, ParseAckError> {
        use ParseAckError::*;

        if !s.starts_with("ACK [") {
            return Err(MissingParts);
        }

        let s = &s[5..];

        let (at_pos, _) = s
            .char_indices()
            .find(|&(_, c)| c == '@')
            .ok_or(MissingParts)?;

        let error_code = s[..at_pos].parse::<u8>()?.into();

        let (clb_pos, _) = s[at_pos + 1..]
            .char_indices()
            .find(|&(_, c)| c == ']')
            .ok_or(MissingParts)?;

        let command_index = s[at_pos + 1..clb_pos].parse::<u16>()?;

        let (obr_pos, _) = s[clb_pos + 1..]
            .char_indices()
            .find(|&(_, c)| c == '{')
            .ok_or(MissingParts)?;

        let (cbr_pos, _) = s[obr_pos + 1..]
            .char_indices()
            .find(|&(_, c)| c == '}')
            .ok_or(MissingParts)?;

        let current_command = s[obr_pos + 1..cbr_pos].to_owned();
        let message = s[cbr_pos..].trim().to_owned();

        Ok(Ack {
            error_code,
            command_index,
            current_command,
            message,
        })
    }
}
