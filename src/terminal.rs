use std::io::{Result, Write};

use bitvec::prelude::{bitvec, BitVec};

bitflags! {
    pub struct Style: u8 {
        const NORMAL = 0;
        const BOLD = 1;
        const ITALIC = 1 << 1;
        const UNDERLINED = 1 << 2;
        const INVERTED = 1 << 3;
    }
}

impl Default for Style {
    fn default() -> Style {
        Style::NORMAL
    }
}

impl Style {
    fn write<W: Write>(self, old: Style, writer: &mut W) -> Result<()> {
        let xor = self ^ old;

        if xor.contains(Style::BOLD) {
            if self.contains(Style::BOLD) {
                write!(writer, "{}", termion::style::NoBold)?;
            } else {
                write!(writer, "{}", termion::style::Bold)?;
            }
        }

        if xor.contains(Style::ITALIC) {
            if self.contains(Style::ITALIC) {
                write!(writer, "{}", termion::style::NoItalic)?;
            } else {
                write!(writer, "{}", termion::style::Italic)?;
            }
        }

        if xor.contains(Style::UNDERLINED) {
            if self.contains(Style::UNDERLINED) {
                write!(writer, "{}", termion::style::NoUnderline)?;
            } else {
                write!(writer, "{}", termion::style::Underline)?;
            }
        }

        if xor.contains(Style::INVERTED) {
            if self.contains(Style::INVERTED) {
                write!(writer, "{}", termion::style::NoInvert)?;
            } else {
                write!(writer, "{}", termion::style::Invert)?;
            }
        }

        Ok(())
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Color4 {
    Black = 30,
    Red = 31,
    Green = 32,
    Yellow = 33,
    Blue = 34,
    Magenta = 35,
    Cyan = 36,
    White = 37,
    BrightBlack = 90,
    BrightRed = 91,
    BrightGreen = 92,
    BrightYellow = 93,
    BrightBlue = 94,
    BrightMagenta = 95,
    BrightCyan = 96,
    BrightWhite = 97,
}

impl Color4 {
    pub fn get_index(self) -> u8 {
        let idx = self as u8 - 30;

        if idx >= 60 {
            idx - 52
        } else {
            idx
        }
    }
}

impl Color4 {
    fn write_fg<W: Write>(self, writer: &mut W) -> Result<()> {
        write!(writer, "\x1B[{}m", self as u8)
    }

    fn write_bg<W: Write>(self, writer: &mut W) -> Result<()> {
        write!(writer, "\x1B[{}m", (self as u8) + 10)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Color8 {
    index: u8,
}

impl Color8 {
    pub fn new_4bit(color: Color4) -> Color8 {
        Color8 {
            index: color.get_index(),
        }
    }

    pub fn new_rgb(r: u8, g: u8, b: u8) -> Color8 {
        assert!(r < 6 && g < 6 && b < 6);

        Color8 {
            index: 16 + 36 * r + 6 * g + b,
        }
    }

    pub fn new_grayscale(index: u8) -> Color8 {
        assert!(index < 24);

        Color8 { index: 232 + index }
    }
}

impl Color8 {
    fn write_fg<W: Write>(self, writer: &mut W) -> Result<()> {
        write!(writer, "\x1B[38;5;{}m", self.index)
    }

    fn write_bg<W: Write>(self, writer: &mut W) -> Result<()> {
        write!(writer, "\x1B[48;5;{}m", self.index)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Color24 {
    pub r: u8,
    pub g: u8,
    pub b: u8,
}

impl Color24 {
    pub fn new(r: u8, g: u8, b: u8) -> Color24 {
        Color24 { r, g, b }
    }
}

impl Color24 {
    fn write_fg<W: Write>(self, writer: &mut W) -> Result<()> {
        write!(writer, "\x1B[38;2;{};{};{}m", self.r, self.g, self.b)
    }

    fn write_bg<W: Write>(self, writer: &mut W) -> Result<()> {
        write!(writer, "\x1B[48;2;{};{};{}m", self.r, self.g, self.b)
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Color {
    Color4(Color4),
    Color8(Color8),
    Color24(Color24),
    Default,
}

impl Default for Color {
    fn default() -> Color {
        Color::Default
    }
}

impl Color {
    fn write_fg<W: Write>(self, writer: &mut W) -> Result<()> {
        use Color::*;

        match self {
            Color4(c) => c.write_fg(writer),
            Color8(c) => c.write_fg(writer),
            Color24(c) => c.write_fg(writer),
            Default => write!(writer, "\x1B[39m"),
        }
    }

    fn write_bg<W: Write>(self, writer: &mut W) -> Result<()> {
        use Color::*;

        match self {
            Color4(c) => c.write_bg(writer),
            Color8(c) => c.write_bg(writer),
            Color24(c) => c.write_bg(writer),
            Default => write!(writer, "\x1B[49m"),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct SymbolEntry {
    pub char: char,
    pub background: Color,
    pub foreground: Color,
    pub style: Style,
}

impl Default for SymbolEntry {
    fn default() -> SymbolEntry {
        SymbolEntry {
            char: ' ',
            style: Style::NORMAL,
            background: Color::Default,
            foreground: Color::Default,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct TerminalSize {
    pub width: u16,
    pub height: u16,
}

impl TerminalSize {
    pub fn new(width: u16, height: u16) -> TerminalSize {
        TerminalSize { width, height }
    }

    pub fn num_entries(self) -> usize {
        (self.width as usize) * (self.height as usize)
    }
}

#[derive(Debug, Clone)]
pub struct TerminalBuffer {
    entries: Vec<SymbolEntry>,
    size: TerminalSize,
    current_background: Color,
    current_foreground: Color,
    current_style: Style,
}

impl TerminalBuffer {
    pub fn new(size: TerminalSize) -> TerminalBuffer {
        TerminalBuffer {
            entries: vec![SymbolEntry::default(); size.num_entries()],
            size,
            current_background: Color::Default,
            current_foreground: Color::Default,
            current_style: Style::NORMAL,
        }
    }

    pub fn set_background(&mut self, background: Color) {
        self.current_background = background;
    }

    pub fn set_foreground(&mut self, foreground: Color) {
        self.current_foreground = foreground;
    }

    pub fn get_background(&self) -> Color {
        self.current_background
    }

    pub fn get_foreground(&self) -> Color {
        self.current_foreground
    }

    pub fn set_style(&mut self, style: Style) {
        self.current_style = style;
    }

    pub fn reset_style(&mut self) {
        self.current_style = Style::NORMAL;
    }

    pub fn get_style(&self) -> Style {
        self.current_style
    }

    pub fn set(&mut self, x: u16, y: u16, char: char) {
        let index = self.index(x, y);
        self.entries[index] = SymbolEntry {
            char,
            background: self.current_background,
            foreground: self.current_foreground,
            style: self.current_style,
        };
    }

    pub fn get_index(&mut self, index: usize) -> SymbolEntry {
        self.entries[index]
    }

    pub fn get(&mut self, x: u16, y: u16) -> SymbolEntry {
        self.entries[self.index(x, y)]
    }

    pub fn get_size(&self) -> TerminalSize {
        self.size
    }

    pub fn clear(&mut self) {
        for entry in &mut self.entries {
            *entry = SymbolEntry::default();
        }
    }

    fn resize(&mut self, new_size: TerminalSize) {
        self.clear();
        self.size = new_size;
        self.entries
            .resize(new_size.num_entries(), SymbolEntry::default());
    }

    fn index(&self, x: u16, y: u16) -> usize {
        assert!(x < self.size.width && y < self.size.height);
        (x as usize) + (y as usize) * (self.size.width as usize)
    }
}

struct BufferDifference {
    changes: BitVec,
    size: TerminalSize,
    is_identical: bool,
}

impl BufferDifference {
    fn new(size: TerminalSize) -> BufferDifference {
        BufferDifference {
            changes: bitvec![0; size.num_entries()],
            size,
            is_identical: true,
        }
    }

    fn set_changed(&mut self, x: u16, y: u16) {
        let index = self.index(x, y);
        self.changes.set(index, true);
        self.is_identical = false;
    }

    fn set_index_changed(&mut self, index: usize) {
        self.changes.set(index, true);
        self.is_identical = false;
    }

    fn has_changed(&mut self, x: u16, y: u16) -> bool {
        let index = self.index(x, y);
        self.changes[index]
    }

    fn clear(&mut self) {
        self.changes.set_all(false);
        self.is_identical = true;
    }

    fn resize(&mut self, new_size: TerminalSize) {
        self.clear();
        self.size = new_size;
        self.changes.resize(new_size.num_entries(), false);
    }

    fn index(&self, x: u16, y: u16) -> usize {
        assert!(x < self.size.width && y < self.size.height);
        (x as usize) + (y as usize) * (self.size.width as usize)
    }
}

fn compare_buffers(a: &TerminalBuffer, b: &TerminalBuffer, diff: &mut BufferDifference) {
    assert_eq!(a.size, b.size);
    assert_eq!(a.size, diff.size);

    for (i, (a_entry, b_entry)) in a.entries.iter().zip(&b.entries).enumerate() {
        if a_entry != b_entry {
            diff.set_index_changed(i);
        }
    }
}

pub struct Terminal {
    old: TerminalBuffer,
    new: TerminalBuffer,
    diff: BufferDifference,
    should_full_redraw: bool,
}

impl Terminal {
    pub fn new(size: TerminalSize) -> Terminal {
        Terminal {
            old: TerminalBuffer::new(size),
            new: TerminalBuffer::new(size),
            diff: BufferDifference::new(size),
            should_full_redraw: true,
        }
    }

    pub fn resize(&mut self, new_size: TerminalSize) {
        if self.new.size == new_size {
            return;
        }

        self.new.resize(new_size);
        self.diff.resize(new_size);
        self.should_full_redraw = true;
    }

    pub fn get_buffer(&mut self) -> &mut TerminalBuffer {
        &mut self.new
    }

    pub fn swap_buffers<W: Write>(&mut self, writer: &mut W) -> Result<()> {
        if self.should_full_redraw {
            self.full_redraw(writer)
        } else {
            self.partial_redraw(writer)
        }?;

        if self.new.size != self.old.size {
            self.old.resize(self.new.size);
        }

        std::mem::swap(&mut self.old, &mut self.new);

        Ok(())
    }

    fn partial_redraw<W: Write>(&mut self, writer: &mut W) -> Result<()> {
        compare_buffers(&self.old, &self.new, &mut self.diff);

        if self.diff.is_identical {
            return Ok(());
        }

        write_reset(writer)?;

        let mut col = 0;
        let mut line = 0;
        let mut cursor_col = 0;
        let mut state = CurrentState::default();

        for (should_redraw, &entry) in self.diff.changes.iter().zip(&self.new.entries) {
            if should_redraw {
                if cursor_col != col {
                    write!(writer, "{}", termion::cursor::Right(col - cursor_col))?;
                    cursor_col = col;
                }

                write_entry(&mut state, entry, writer)?;
                writer.flush()?;
                cursor_col += 1;
            }

            col += 1;

            if col == self.new.size.width && line < self.new.size.height - 1 {
                #[allow(clippy::write_with_newline)]
                write!(writer, "\r\n")?;

                col = 0;
                cursor_col = 0;
                line += 1;
            }
        }

        self.diff.clear();

        Ok(())
    }

    fn full_redraw<W: Write>(&mut self, writer: &mut W) -> Result<()> {
        write_reset(writer)?;

        let mut col = 0;
        let mut line = 0;
        let mut state = CurrentState::default();

        for &entry in &self.new.entries {
            write_entry(&mut state, entry, writer)?;
            col += 1;

            if col == self.new.size.width && line < self.new.size.height - 1 {
                #[allow(clippy::write_with_newline)]
                write!(writer, "\r\n")?;

                col = 0;
                line += 1;
            }
        }

        self.should_full_redraw = false;

        Ok(())
    }
}

#[derive(Default)]
struct CurrentState {
    background: Color,
    foreground: Color,
    style: Style,
}

fn write_entry<W: Write>(
    state: &mut CurrentState,
    entry: SymbolEntry,
    writer: &mut W,
) -> Result<()> {
    if entry.background != state.background {
        state.background = entry.background;
        entry.background.write_bg(writer)?;
    }

    if entry.foreground != state.foreground {
        state.foreground = entry.foreground;
        entry.foreground.write_fg(writer)?;
    }

    entry.style.write(state.style, writer)?;
    state.style = entry.style;

    write!(writer, "{}", entry.char)
}

fn write_reset<W: Write>(writer: &mut W) -> Result<()> {
    write!(
        writer,
        "{}{}",
        termion::style::Reset,
        termion::cursor::Goto(1, 1)
    )
}
