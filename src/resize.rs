use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use crate::terminal::{Terminal, TerminalSize};

pub struct ResizeHandler {
    has_resized: Arc<AtomicBool>,
}

impl ResizeHandler {
    pub fn init() -> ResizeHandler {
        let has_resized = Arc::new(AtomicBool::new(false));

        signal_hook::flag::register(signal_hook::SIGWINCH, has_resized.clone())
            .expect("Failed to setup SIGWINCH handler");

        ResizeHandler { has_resized }
    }

    pub fn get_size(&self) -> TerminalSize {
        let (width, height) = termion::terminal_size().expect("Failed to get terminal size");
        TerminalSize::new(width, height)
    }

    pub fn update(&self, terminal: &mut Terminal) {
        let has_resized = self
            .has_resized
            .compare_and_swap(true, false, Ordering::Relaxed);

        if !has_resized {
            terminal.resize(self.get_size());
        }
    }
}
