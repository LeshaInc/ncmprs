#![deny(rust_2018_idioms)]
#![deny(unsafe_code)]
#![allow(dead_code)]

#[macro_use]
extern crate bitflags;
#[macro_use]
extern crate newtype;
#[macro_use]
extern crate derivative;

mod client;
mod resize;
mod terminal;

use std::io::{self, BufWriter, Write};

use termion::raw::IntoRawMode;
use termion::screen::AlternateScreen;

use self::resize::ResizeHandler;
use self::terminal::{Terminal, TerminalBuffer, TerminalSize};

fn main() {
    let resizer = ResizeHandler::init();
    let mut terminal = Terminal::new(resizer.get_size());

    let stdout = io::stdout();
    let buf = BufWriter::with_capacity(32000, stdout.lock())
        .into_raw_mode()
        .unwrap();

    let mut buf = AlternateScreen::from(buf);

    let instant = std::time::Instant::now();

    let mut i = 0;

    loop {
        resizer.update(&mut terminal);

        draw(terminal.get_buffer(), i);
        i += 1;

        terminal.swap_buffers(&mut buf).unwrap();
        buf.flush().unwrap();

        if instant.elapsed() > std::time::Duration::from_secs(10) {
            break;
        }
    }
}

fn draw(buf: &mut TerminalBuffer, i: i32) {
    let TerminalSize { width, height } = buf.get_size();

    buf.clear();

    for x in 0..width {
        buf.set(x, 0, 'x');
        buf.set(x, height - 1, 'x');
    }

    let s = format!("{}", i);
    for (i, c) in s.chars().enumerate() {
        buf.set(2 + i as u16, 2, c);
    }

    for y in 0..height {
        buf.set(0, y, 'x');
        buf.set(width - 1, y, 'x');
    }
}
